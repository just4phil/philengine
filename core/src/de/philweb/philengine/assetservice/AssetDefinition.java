package de.philweb.philengine.assetservice;

import com.badlogic.gdx.assets.AssetManager;

import de.philweb.philengine.common.Game;

public abstract class AssetDefinition implements IassetService {

	protected Game game;
	protected AssetService _assetService;
	protected AssetManager m_assetManager;
	
	public AssetDefinition(Game game) {
		this.game = game;
	}
	
	// will be called automatically bei AssetService.initialize
	@Override
	public abstract void defineAssetLoadingTasks();
	
	// will be called automatically bei AssetService.initialize
	@Override
	public void setAssetService(AssetService service) {
		this._assetService = service;
		this.m_assetManager = _assetService.getAssetManager();
	}
	
	public AssetService getAssetService() {
		return _assetService;
	}
}
