package de.philweb.philengine.assetservice;


public interface IassetService {
	
	// will be called automatically bei AssetService.initialize
	public void defineAssetLoadingTasks();
	
	// will be called automatically bei AssetService.initialize
	public void setAssetService(AssetService assetService);
}
