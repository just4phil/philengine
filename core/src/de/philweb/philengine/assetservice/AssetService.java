package de.philweb.philengine.assetservice;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Disposable;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.assetloaders.SkeletonDataLoader;
import com.esotericsoftware.spine.assetloaders.SkeletonDataLoader.SkeletonDataLoaderParameter;

import de.philweb.philengine.common.Game;
import de.philweb.philengine.common.ResolutionFileResolverChooser;
import de.philweb.philengine.common.ResolutionFileResolverChooser.Resolution;

public class AssetService implements Disposable {
				
	private Game game;
//	private static AssetService instance;
	protected static AssetManager _assetManager;
	private IassetService assetsImplementation;
	protected AssetLoadingProgressThread progressThread = null;
	protected float progress = 0f;
	
	// ------- RESOLUTIONS ----------
	private Resolution[] resolutions;
	protected int resolution = 0; // set in assets depending on actual choosen res.
	protected float resolutionFontScale = 1; // set in assets depending on actual choosen res.
	private String resolvedDirectory;
	private String resolvedPath;
	
	//---- asset groups -------------
	ArrayList<AssetGroup> assetGroups;
	
	public AssetService(Game game) {
		this.game = game;
		_assetManager = new AssetManager();
		assetGroups = new ArrayList<AssetGroup>();
	}
	
//	//--- singleton with lazy initialization
//    public static AssetService getInstance(Game game) {
//        if(instance == null){
//            instance = new AssetService(game);
////            instance.setDebug(isDebug);
////            if (isDebug) Gdx.app.log("AssetService", "new AssetService() constructed");
//        }
//        return instance;
//    }
     
    public AssetService initialize(Resolution[] resolutions, IassetService assetsImplementation) {
    	this.resolutions = resolutions;
    	this.assetsImplementation = assetsImplementation;
    	this.assetsImplementation.setAssetService(this);
    	this.assetsImplementation.defineAssetLoadingTasks();
    	return this;
    }
    
    public void loadAssetGroup(int groupID) {	
    	loadAssetGroup(groupID, false);
    }
    public void loadAssetGroup(int groupID, boolean finishLoading) {	
    	loadAssetGroup(groupID, finishLoading, null);
    }
    public void loadAssetGroup(int groupID, boolean finishLoading, IfinishedLoadingCallback callback) {
    	AssetGroup group = getAssetGroup(groupID);
    	loadAssetGroup(group, finishLoading, callback);
    }
    private void loadAssetGroup(AssetGroup group, boolean finishLoading, IfinishedLoadingCallback callback) {
    	if (callback != null) group.setCallback(callback);
    	loadAssetGroup(group, finishLoading);
    }
    	
    private void loadAssetGroup(AssetGroup group, boolean finishLoading) {		
		ResolutionFileResolverChooser resolver;
		int x, y;
		progress = 0f;		
		resolvedDirectory = "";
		resolvedPath = "";
		if (game.getDebugMode()) Gdx.app.log("AssetService", "start loading group: " + group.groupID);
		
		for (x = 0; x < group.loadingTasks.size(); x++) {
			AssetLoadingTask task = group.loadingTasks.get(x);
			if (game.getDebugMode()) Gdx.app.log("AssetService", "start loading task: " + task.taskID);
						
			switch (task.typeID) {
			
			case AssetLoadingTask.TextureAtlasLoaderTask:
				if (task.chooserStrategy != null && task.resolutions != null) {
					resolver = new ResolutionFileResolverChooser(new InternalFileHandleResolver(), task.chooserStrategy, task.resolutions);
					resolvedDirectory = resolver.resolveDirectory();
					resolvedPath = resolver.resolve(task.filePath).path();
					_assetManager.setLoader(TextureAtlas.class, new TextureAtlasLoader(resolver));
					_assetManager.load(task.filePath, TextureAtlas.class);
				}
				else {
					resolvedDirectory = task.filePath;
					resolvedPath = task.filePath;
					_assetManager.load(task.filePath, TextureAtlas.class);
				}
				break;
			
			case AssetLoadingTask.TextureLoaderTask:
				if (task.chooserStrategy != null && task.resolutions != null) {
					resolver = new ResolutionFileResolverChooser(new InternalFileHandleResolver(), task.chooserStrategy, task.resolutions);
					resolvedDirectory = resolver.resolveDirectory();
					resolvedPath = resolver.resolve(task.filePath).path();
					_assetManager.setLoader(Texture.class, new TextureLoader(resolver));
					if (task.param != null) _assetManager.load(task.filePath, Texture.class, task.param);
					else _assetManager.load(task.filePath, Texture.class);
				}
				else {
					resolvedDirectory = task.filePath;
					resolvedPath = task.filePath;
					_assetManager.load(task.filePath, Texture.class);
				}
				break;
				
			case AssetLoadingTask.OggSoundLoaderTask:
				for (y = 0; y < task.filenames.size(); y++) {
					_assetManager.load(task.filePath + task.filenames.get(y), Sound.class);
					if (game.getDebugMode()) Gdx.app.log("AssetService", "loaded Ogg Sound: " + task.filenames.get(y));
				}
				resolvedDirectory = task.filePath;
				resolvedPath = task.filePath;
				break;	
				
			case AssetLoadingTask.OggMusicLoaderTask:
				for (y = 0; y < task.filenames.size(); y++) {
					_assetManager.load(task.filePath + task.filenames.get(y), Music.class);
					if (game.getDebugMode()) Gdx.app.log("AssetService", "loaded Ogg Music: " + task.filenames.get(y));
				}
				resolvedDirectory = task.filePath;
				resolvedPath = task.filePath;
				break;	
				
			case AssetLoadingTask.SpineLoaderTask:
				if (task.chooserStrategy != null && task.resolutions != null) {
					resolver = new ResolutionFileResolverChooser(new InternalFileHandleResolver(), task.chooserStrategy, task.resolutions);
					resolvedDirectory = resolver.resolveDirectory();
					resolvedPath = resolver.resolve(task.dirPath).path();			        
			        _assetManager.setLoader(SkeletonData.class, new SkeletonDataLoader(resolver));
			        SkeletonDataLoaderParameter parameter = new SkeletonDataLoaderParameter(resolvedPath);
			        _assetManager.load(task.filePath, SkeletonData.class, parameter);
				}
				else {
					resolvedDirectory = task.dirPath;
					resolvedPath = task.dirPath;
					_assetManager.load(task.filePath, TextureAtlas.class);
				}
				break;

			// FIXME:  skinfile haengt ab vom atlas der von der resolution abhaengt!!
			case AssetLoadingTask.SkinLoaderTask:
				_assetManager.load(task.filePath, Skin.class);
				break;
				
			//==================== TODO ==============================================================
				
			case AssetLoadingTask.BitmapFontLoaderTask:
//				m_assetManager.load(DIR_MUSIC + "PixelPeekerPolkaSlower.ogg", Music.class);
				break;	
			}
			//-------------------------------------------------------------
			
			if (game.getDebugMode()) {
				Gdx.app.log("AssetService", "resolvedDirectory: " + resolvedDirectory);
				Gdx.app.log("AssetService", "resolvedPath: " + resolvedPath);
			}
		}
		//--- assetmanager is setup ... now start sync or async loading ---------
		if (finishLoading) {
			if (game.getDebugMode()) Gdx.app.log("AssetService", "started synced / blocked loading of group: " + group.groupID);
			_assetManager.finishLoading();
			progress = _assetManager.getProgress();
			if (game.getDebugMode()) Gdx.app.log("AssetService", "assetManager.getProgress(): " + progress);
			
			if (group.callback != null) {
				group.callback.onFinishedLoading(resolvedDirectory);
				return;
			}
		}
		else{
			progressThread = new AssetLoadingProgressThread(this, group.callback, resolvedDirectory);
			progressThread.setDaemon(true);
			progressThread.start();
			if (game.getDebugMode()) Gdx.app.log("AssetService", "started thread for group: " + group.groupID);
		}
	}	
        
	public void disposeAtlas(String atlasToDispose) {
		if (atlasToDispose != null && !atlasToDispose.equals("")) {
			if (game.getDebugMode()) Gdx.app.log("AssetService", "getLoadedAssets before dispose: "  + _assetManager.getLoadedAssets());
			_assetManager.unload(atlasToDispose);
			Gdx.app.log("AssetService", "unloaded atlas: " + atlasToDispose);
			Gdx.app.log("AssetService", "getLoadedAssets after dispose: "  + _assetManager.getLoadedAssets());
		}
		else if (game.getDebugMode()) Gdx.app.log("AssetService", "disposeAtlas: no atlas found");
	}
	
	public AssetManager getAssetManager () {
		return _assetManager;
	}
	public AssetGroup getAssetGroup(int groupID) {		
		for (int i = 0; i < assetGroups.size(); i++) {
			if (assetGroups.get(i).groupID == groupID) return assetGroups.get(i); 
		}
		return null;
	}
	public void addAssetGroup (AssetGroup group) {
		assetGroups.add(group);
	}
	public Resolution[] getResolutions() {
		return resolutions;
	}
	public int getResolution() {
		return resolution;
	}
	public IassetService getAssets() {
		return assetsImplementation;
	}
	public void setResolution (int res) {
		resolution = res;
	}
	public float getResolutionFontScale () {
		return resolutionFontScale;
	}
	public void setResolutionFontScale (float res) {
		resolutionFontScale = res;
	}
	public float getProgress() {
		return progress;
	}

	@Override
	public void dispose() {
		_assetManager.dispose();
		assetGroups.clear();
		assetGroups = null;
		assetsImplementation = null;
		progressThread = null;
		resolutions = null;
//		instance = null;
	}
	
}
