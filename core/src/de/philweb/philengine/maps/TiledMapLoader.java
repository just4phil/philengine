package de.philweb.philengine.maps;

import java.util.ArrayList;

import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;

public class TiledMapLoader {

	/** returns all mapobjects of a layer as list 
	 * width, height, x and y are converted to meters by originalTiledmap Pixel per meters */
	public static ArrayList<RectangleMapObject> getRectangleMapObjects(MapService mapService, String layerName, float originalTildemapPPM) {
		TiledMap tMap = mapService.getTiledMapManager().tiledMap;
		ArrayList<RectangleMapObject> list = new ArrayList<RectangleMapObject>();
		RectangleMapObject rectangleMapObject;
		
		int anz_objectGroups = tMap.getLayers().getCount();	
		for (int counter_groups = 0; counter_groups < anz_objectGroups; counter_groups++) {
			if (tMap.getLayers().get(counter_groups).getName().equals(layerName)) {
				int anz_objekte = tMap.getLayers().get(counter_groups).getObjects().getCount();	
				for (int counter = 0; counter < anz_objekte; counter++) {
					rectangleMapObject = (RectangleMapObject)tMap.getLayers().get(counter_groups).getObjects().get(counter);
					float w = rectangleMapObject.getRectangle().getWidth() / originalTildemapPPM;
					float h = rectangleMapObject.getRectangle().getHeight() / originalTildemapPPM;
					float x = mapService.game.getGameConfig().getPLAYFIELDOFFSETX() + (rectangleMapObject.getRectangle().x / originalTildemapPPM) + (w * 0.5f);
					float y = mapService.game.getGameConfig().getPLAYFIELDOFFSETY() + (rectangleMapObject.getRectangle().y / originalTildemapPPM) + (h * 0.5f);
					rectangleMapObject.getRectangle().width = w;
					rectangleMapObject.getRectangle().height = h;
					rectangleMapObject.getRectangle().x = x;
					rectangleMapObject.getRectangle().y = y;
					list.add(rectangleMapObject);
				}
			}
		}
		return list;
	}
	
//	public static ArrayList<RectangleMapObject> getRectangleMapObjects(MapService mapService, String layerName, float originalTildemapPPM) {
//		
//		TiledMap tMap = mapService.getTiledMapManager().tiledMap;
//		ArrayList<RectangleMapObject> list = new ArrayList<RectangleMapObject>();
//		RectangleMapObject rectangleMapObject;
//		float x, y, w, h;		
//		
//		int anz_objectGroups = tMap.getLayers().getCount();	
//		for (int counter_groups = 0; counter_groups < anz_objectGroups; counter_groups++) {
//			if (tMap.getLayers().get(counter_groups).getName().equals(layerName)) {
//				int anz_objekte = tMap.getLayers().get(counter_groups).getObjects().getCount();	
//				
//				for (int counter = 0; counter < anz_objekte; counter++) {
//					rectangleMapObject = (RectangleMapObject)tMap.getLayers().get(counter_groups).getObjects().get(counter);
//					w = (rectangleMapObject.getRectangle().width) / originalTildemapPPM;
//					h = (rectangleMapObject.getRectangle().height) / originalTildemapPPM;
//					x = (rectangleMapObject.getRectangle().x / originalTildemapPPM) + (w * 0.5f);
//					y = (rectangleMapObject.getRectangle().y / originalTildemapPPM) + (h * 0.5f);
//					
//					GameObject objekt  = new GameObject();
//					objekt.position_m.x = GameConfig.PLAYFIELDOFFSETX + x;
//					objekt.position_m.y = GameConfig.PLAYFIELDOFFSETY + y;
//					objekt.assetName = tMap.getLayers().get(counter_groups).getObjects().get(counter).getName();
//					objectList.add(objekt);
//				}
//			}
//		}
//
//		return objectList;
//	}
}
