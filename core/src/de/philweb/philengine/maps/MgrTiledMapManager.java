package de.philweb.philengine.maps;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;

import de.philweb.philengine.common.BaseGameConfig;
import de.philweb.philengine.common.Game;
import de.philweb.philengine.common.MgrCameraManager;
import de.philweb.philengine.gameobjects.GoGameObject;

public class MgrTiledMapManager implements Disposable {
	
	Game game;
	MapService map;
	MgrCameraManager camMgr;
	BaseGameConfig gameConfig;
	SpriteBatch batch;
	OrthographicCamera GAMEcam;
	public TiledMap tiledMap;
	boolean blendingEnabled; // this must be set if the tiledmap needs transparency
	
	//----- debug rendering ------
	boolean debugRender;
	ShapeRenderer shapeRenderer;
	float TILESIZE_MTR;
	
	// TiledMap Offset
//	float offX, offY;
	
	//----- shadow ------------
	float SHADOWALPHA;
	float shadowOffsetX;
	float shadowOffsetY;
	
//	private TiledMap2Box2dConverter tiledMap2Box2dConverter;
//	private TmxMapLoader tmxMapLoader;
	private PeOrthogonalTiledMapRenderer mapRenderer;
//	private CachedOrthogonalTiledMapRenderer mapRenderer;
		
	public MgrTiledMapManager(MapService map) {
		this.game = map.game;
		this.map = map;
		this.camMgr = game.getCameraManager();
		this.batch = game.getSpritebatch();
		this.gameConfig = game.getGameConfig();
		this.GAMEcam = camMgr.getGAMECamera();
		
//		offX = 0f;
//		offY = 0f;
		
		SHADOWALPHA = 0f;
		shadowOffsetX = 0f;
		shadowOffsetY = 0f;
		blendingEnabled = false;
		tiledMap = null;
		debugRender = false;
		TILESIZE_MTR = 0f;
		
		mapRenderer = new PeOrthogonalTiledMapRenderer(game, null, 1 / gameConfig.getVirtualPxPerMtr(), batch);
//		mapRenderer = new CachedOrthogonalTiledMapRenderer(game, tiledMap, 1 / game.VIRTUAL_PXPERMTR);	// ich glaube die ppm muessen hart auf 32 eingestellt werden weil die TMX files für alle auflösungen die gleichen sind
	}
	
	public MgrTiledMapManager setTiledMap(TiledMap tiledMap) {
		setTiledMap(tiledMap, 0f, 0f, 0f, false);
		return this;
	}
		
	public MgrTiledMapManager setTiledMap(TiledMap tiledMap, float SHADOWALPHA, float shadowOffsetX, float shadowOffsetY, boolean blendingEnabled) {
		this.tiledMap = tiledMap;
		mapRenderer.setMap(tiledMap);
		enableShadow(SHADOWALPHA, shadowOffsetX, shadowOffsetY);
		enableBlending(blendingEnabled);
		return this;
	}
	
//	public MgrTiledMapManager setTiledMapOffset(float offX, float offY) {
//		this.offX = offX;
//		this.offY = offY;
//		return this;
//	}
	
	public MgrTiledMapManager enableShadow(float SHADOWALPHA, float shadowOffsetX, float shadowOffsetY) {
		this.SHADOWALPHA = SHADOWALPHA;
		this.shadowOffsetX = shadowOffsetX;
		this.shadowOffsetY = shadowOffsetY;
		return this;
	}
	
	// this must be set if the tiledmap needs transparency
	public MgrTiledMapManager enableBlending(boolean blendingEnabled) {
		this.blendingEnabled = blendingEnabled;
		return this;
	}
	
	/** enables debug rendering with shapes (lines) 
	 * use this only for testing / debugging
	 * a new ShapeRenderer will be created
	 */
	public MgrTiledMapManager enableDebugRendering() {
		enableDebugRendering(null);
		return this;
	}
	
	/** enables debug rendering with shapes (lines) 
	 * use this only for testing / debugging
	 * @param shapeRenderer: leave this as NULL and a new ShapeRenderer will be created
	 */
	public MgrTiledMapManager enableDebugRendering(ShapeRenderer shapeRenderer) {
		TILESIZE_MTR = gameConfig.getTILESIZE_MTR();
		this.debugRender = true;
		this.shapeRenderer = shapeRenderer;
		if (shapeRenderer == null) {
			this.shapeRenderer = new ShapeRenderer();
		}
		return this;
	}
	
	public void render() {
		
		camMgr.useGAMECam();
				
		//-------------------- draw shadow ------------------------------------------------
		
		if (SHADOWALPHA > 0f) {
			
			batch.enableBlending();
			
			//---- move tiledmap-camera to the middle of the playfield -----------------  
			GAMEcam.position.x = (game.getVIRTUAL_WIDTH_GAME() * 0.5f) - gameConfig.getPLAYFIELDOFFSETX() + shadowOffsetX; // - 0.2f;	// shadow
			GAMEcam.position.y = (game.getVIRTUAL_HEIGHT_GAME() * 0.5f) - gameConfig.getPLAYFIELDOFFSETY() + shadowOffsetY; // + 0.3f;	// shadow
			GAMEcam.update();
	        
			//---- render tiledmap -----------------
			mapRenderer.setView(GAMEcam);
			mapRenderer.getBatch().setColor(0f, 0f, 0f, SHADOWALPHA);
			mapRenderer.render();
		}
		
		if (batch.isBlendingEnabled()) {
		
			if (blendingEnabled == false) batch.disableBlending();
		}
		else {
			if (blendingEnabled == true) batch.enableBlending();
		}
	
		//------------- draw normal tiledmap ---------------------------------------------------------------
		
		//---- move tiledmap-camera to the middle of the playfield -----------------
		GAMEcam.position.x = (game.getVIRTUAL_WIDTH_GAME() * 0.5f) - gameConfig.getPLAYFIELDOFFSETX();
		GAMEcam.position.y = (game.getVIRTUAL_HEIGHT_GAME() * 0.5f) - gameConfig.getPLAYFIELDOFFSETY();		
		
//        GAMEcam.position.x = (gameConfig.getVIRTUAL_WIDTH_GAME() * 0.5f) - ((gameConfig.getVIRTUAL_WIDTH_GAME() * 0.5f) - (gameConfig.getPLAYFIELDWIDTH() * 0.5f));
//        GAMEcam.position.y = (gameConfig.getVIRTUAL_HEIGHT_GAME() * 0.5f) - ((gameConfig.getVIRTUAL_HEIGHT_GAME() * 0.5f) - (gameConfig.getPLAYFIELDHEIGHT() * 0.5f));		
		GAMEcam.update();
        
		//---- render tiledmap -----------------
		mapRenderer.setView(GAMEcam);
		mapRenderer.getBatch().setColor(1f, 1f, 1f, 1f);
		mapRenderer.render();
		
		//---- move tiledmap-camera back to the middle of the viewport for correct unproject-coords -----------------
        GAMEcam.position.x = game.getVIRTUAL_WIDTH_GAME() * 0.5f;
        GAMEcam.position.y = game.getVIRTUAL_HEIGHT_GAME() * 0.5f;
//      GAMEcam.update();
//      camMgr.useGAMECam(); 
        
        //----------- debugRender ---------------------------------------------------------------
        
        if (debugRender) {
        	
//    		camMgr.useHUDCam();
    		camMgr.getHUDCamera().position.x = (game.getVIRTUAL_WIDTH_HUD() * 0.5f) - gameConfig.getPLAYFIELDOFFSETX() * gameConfig.getREAL_PXPERMTR();
    		camMgr.getHUDCamera().position.y = (game.getVIRTUAL_HEIGHT_HUD() * 0.5f) - gameConfig.getPLAYFIELDOFFSETY() * gameConfig.getREAL_PXPERMTR();
    		camMgr.getHUDCamera().update();
    		
    		Gdx.gl.glEnable(GL20.GL_BLEND);
    		shapeRenderer.setProjectionMatrix(camMgr.getHUDCamera().combined);
    		shapeRenderer.begin(ShapeType.Line);
    		
    		//--- debug draw tiledmap cells ------------
    		shapeRenderer.setColor(1f, 1f, 1f, 0.075f);
    		for (int x = 0; x < gameConfig.getTILEDMAPSIZEINTILESX(); x++) {
    			for (int y = 0; y < gameConfig.getTILEDMAPSIZEINTILESY(); y++) {
    				shapeRenderer.rect(x * TILESIZE_MTR * gameConfig.getREAL_PXPERMTR(), y * TILESIZE_MTR * gameConfig.getREAL_PXPERMTR(),
    					TILESIZE_MTR * gameConfig.getREAL_PXPERMTR(), TILESIZE_MTR * gameConfig.getREAL_PXPERMTR());
    			}
    		}
    		shapeRenderer.end();
    		Gdx.gl.glDisable(GL20.GL_BLEND);
    		
    		//----- re-position cam to center
    		camMgr.getHUDCamera().position.x = (game.getVIRTUAL_WIDTH_HUD() * 0.5f);
    		camMgr.getHUDCamera().position.y = (game.getVIRTUAL_HEIGHT_HUD() * 0.5f);
    		camMgr.getHUDCamera().update();
        }
	}
	//=============================================================================
	
	public static void convertTiles2Boxes(World box2dWorld, TiledMap map, float PPM, float playfieldOffsetX, float playfieldOffsetY, String name) {
		
//		int ID;
		TiledMapTileLayer layer = (TiledMapTileLayer)map.getLayers().get(0);
//		
	    for(int y = 0; y <= layer.getHeight() -1; y++) {
	    	for(int x = 0; x <= layer.getWidth() -1; x++) {
	    		Cell cell = layer.getCell(x, y);
	    		if(cell != null) {
	    			
//	    			ID = cell.getTile().getProperties().get("type", defaultID, int.class);
	    			
	    			PhysicsBodyFactory.addStaticTileBodyAndFixture(name, box2dWorld,  
	    				playfieldOffsetX + ((x + 1) * layer.getTileWidth() / PPM),
	    				playfieldOffsetY + ((y + 1) * layer.getTileHeight() / PPM), 
	    				layer.getTileWidth() / PPM, 
	    				layer.getTileHeight() / PPM,
	    				0, 0);
	    		} 
	      	}
	    }
	}
	
	//======================================================
	
	public int getMapWidthInTiles() {
//		return map.getProperties().get("width", Integer.class);
		
		return game.getTILEDMAPSIZEINTILESX();
	}
	
	public int getMapHeightInTiles() {
//		return map.getProperties().get("height", Integer.class);
		
		return game.getTILEDMAPSIZEINTILESY();
	}
	
	public int getTileWidthInPx() {
//		return map.getProperties().get("tilewidth", Integer.class);
		
		return game.getVIRTUAL_TILESIZE();
	}
	
	public int getTileHeightInPx() {
//		return map.getProperties().get("tileheight", Integer.class);
		
		return game.getVIRTUAL_TILESIZE();
	}
	
	//======================================================
	
		/**
		 * Reads a file describing the collision boundaries that should be set
		 * per-tile and adds static bodies to the boxd world.
		 * 
		 * @param collisionsFile
		 * @param world
		 * @param pixelsPerMeter
		 *            the pixels per meter scale used for this world
		 */
		public GoGameObject createTiledMapBox2DObject(int objectID, String collisionsFile, World box2dWorld, float pixelsPerMeter, float GROUNDBODYOFFSETX, float GROUNDBODYOFFSETY) {
			
			GoGameObject groundBody = new GoGameObject(map, objectID, 0, 0, 0, 0);
			groundBody.addBox2dBody(box2dWorld, BodyDef.BodyType.StaticBody, 0f);
			
			/**
			 * Detect the tiles and dynamically create a representation of the map
			 * layout, for collision detection. Each tile has its own collision
			 * rules stored in an associated file.
			 * 
			 * The file contains lines in this format (one line per type of tile):
			 * tileNumber XxY,XxY XxY,XxY
			 * 
			 * Ex:
			 * 
			 * 3 0x0,31x0 ... 4 0x0,29x0 29x0,29x31
			 * 
			 * For a 32x32 tileset, the above describes one line segment for tile #3
			 * and two for tile #4. Tile #3 has a line segment across the top. Tile
			 * #1 has a line segment across most of the top and a line segment from
			 * the top to the bottom, 30 pixels in.
			 */

			FileHandle fh = Gdx.files.internal(collisionsFile);
			String collisionFile = fh.readString();
			String lines[] = collisionFile.split("\\r?\\n");

			HashMap<Integer, ArrayList<LineSegment>> tileCollisionJoints = new HashMap<Integer, ArrayList<LineSegment>>();

			/**
			 * Some locations on the map (perhaps most locations) are "undefined",
			 * empty space, and will have the tile type 0. This code adds an empty
			 * list of line segments for this "default" tile.
			 */
			tileCollisionJoints.put(Integer.valueOf(0), new ArrayList<LineSegment>());

			for (int n = 0; n < lines.length; n++) {
				String cols[] = lines[n].split(" ");
				int tileNo = Integer.parseInt(cols[0]);

				ArrayList<LineSegment> tmp = new ArrayList<LineSegment>();

				for (int m = 1; m < cols.length; m++) {
					String coords[] = cols[m].split(",");

					String start[] = coords[0].split("x");
					String end[] = coords[1].split("x");

					tmp.add(new LineSegment(Integer.parseInt(start[0]), Integer.parseInt(start[1]), 
							Integer.parseInt(end[0]), Integer.parseInt(end[1])));
				}

				tileCollisionJoints.put(Integer.valueOf(tileNo), tmp);
			}

			ArrayList<LineSegment> collisionLineSegments = new ArrayList<LineSegment>();
			
			TiledMapTileLayer layer = (TiledMapTileLayer)tiledMap.getLayers().get(0);
			int tileType;
			
			for (int y = 0; y < getMapHeightInTiles(); y++) {
				for (int x = 0; x < getMapWidthInTiles(); x++) {

					Cell cell = layer.getCell(x, y);
					if (cell != null) {
						tileType = cell.getTile().getId();
					}
					else tileType = 0;
//					Gdx.app.log("y: " + y + " // x: " + x, "tileType: " + tileType);
					
					for (int n = 0; n < tileCollisionJoints.get(Integer.valueOf(tileType)).size(); n++) {
						LineSegment lineSeg = tileCollisionJoints.get(Integer.valueOf(tileType)).get(n);

//						original:
//						addOrExtendCollisionLineSegment(
//								x * getTileWidthInPx() + lineSeg.start().x, 
//								y * getTileHeightInPx()	- lineSeg.start().y + 32, // Tanks.VIRTUAL_TILESIZE ??
//								x * getTileWidthInPx() + lineSeg.end().x, 
//								y * getTileHeightInPx()	- lineSeg.end().y + 32, 
//								collisionLineSegments);
						
						//---- hier den offset entfernt und dafuer unten ergaenzt
						addOrExtendCollisionLineSegment(
								x * getTileWidthInPx() + lineSeg.start().x, 
								y * getTileHeightInPx()	- lineSeg.start().y + 0, 
								x * getTileWidthInPx() + lineSeg.end().x, 
								y * getTileHeightInPx()	- lineSeg.end().y + 0, 
								collisionLineSegments);
						
					}
				}
			}
			
			for (LineSegment lineSegment : collisionLineSegments) {
				EdgeShape environmentShape = new EdgeShape();

				Vector2 vectStart = lineSegment.start().scl(1 / pixelsPerMeter);
				
//				vectStart.x = vectStart.x + game.getPLAYFIELDOFFSETX() + game.getGROUNDBODYOFFSETX();	//---- hier den offset ergaenzt
//				vectStart.y = vectStart.y - game.getPLAYFIELDOFFSETY() + game.getGROUNDBODYOFFSETY();	//---- hier den offset ergaenzt
				
				vectStart.x = vectStart.x + game.getGameConfig().getPLAYFIELDOFFSETX() + GROUNDBODYOFFSETX;
				vectStart.y = vectStart.y + game.getGameConfig().getPLAYFIELDOFFSETY() + GROUNDBODYOFFSETY;
				
				Vector2 vectEnd = lineSegment.end().scl(1 / pixelsPerMeter);

//				vectEnd.x = vectEnd.x + game.getPLAYFIELDOFFSETX() + game.getGROUNDBODYOFFSETX();	//---- hier den offset ergaenzt
//				vectEnd.y = vectEnd.y - game.getPLAYFIELDOFFSETY() + game.getGROUNDBODYOFFSETY();	//---- hier den offset ergaenzt
				
				vectEnd.x = vectEnd.x + game.getGameConfig().getPLAYFIELDOFFSETX() + GROUNDBODYOFFSETX;
				vectEnd.y = vectEnd.y + game.getGameConfig().getPLAYFIELDOFFSETY() + GROUNDBODYOFFSETY;
				
				environmentShape.set(vectStart, vectEnd);
//				environmentShape.set(lineSegment.start().scl(1 / pixelsPerMeter), lineSegment.end().scl(1 / pixelsPerMeter));

				groundBody.getBody().createFixture(environmentShape, 0);
				environmentShape.dispose();
			}

			
			//--- f�r kollisionserkennung im contactlistener --------------------------------------------------------
//			
//			Array<Fixture> groundFixtures = new Array<Fixture>();
//			groundFixtures = groundBody.getBody().getFixtureList();
//			for (int i = 0; i < groundFixtures.size; i++) {		
//				
//				groundFixtures.get(i).setUserData(ObjectID.FIX_GROUND);	
	//
//				Filter maskFilter = new Filter();
//				maskFilter.categoryBits = ObjectID.CATEGORY_SCENERY;
	//
//				maskFilter.maskBits = 
//						ObjectID.CATEGORY_BUBBLE | ObjectID.CATEGORY_CANNONBALL | ObjectID.CATEGORY_COLLECTABLE | 
//						ObjectID.CATEGORY_FIREBALL | ObjectID.CATEGORY_GROUNDSENSOR | ObjectID.CATEGORY_MONSTER | 
//						ObjectID.CATEGORY_PLAYER | ObjectID.CATEGORY_BUBBLESENSOR;
//				
//				groundFixtures.get(i).setFilterData(maskFilter);	
	//
//				
//			}	//-------------------------------------------------------------------------------------------------------
			
			
			
			/**
			 * Drawing a boundary around the entire map. We can't use a box because
			 * then the world objects would be inside and the physics engine would
			 * try to push them out.
			 */

//			EdgeShape mapBounds = new EdgeShape();
//			mapBounds.set(new Vector2(0.0f, 0.0f), new Vector2(getWidth() / pixelsPerMeter, 0.0f));
//			groundBody.createFixture(mapBounds, 0);
	//
//			mapBounds.set(new Vector2(0.0f, getHeight() / pixelsPerMeter), new Vector2(getWidth() / pixelsPerMeter, getHeight() / pixelsPerMeter));
//			groundBody.createFixture(mapBounds, 0);
	//
//			mapBounds.set(new Vector2(0.0f, 0.0f), new Vector2(0.0f, getHeight() / pixelsPerMeter));
//			groundBody.createFixture(mapBounds, 0);
	//
//			mapBounds.set(new Vector2(getWidth() / pixelsPerMeter, 0.0f), new Vector2(getWidth() / pixelsPerMeter, getHeight() / pixelsPerMeter));
//			groundBody.createFixture(mapBounds, 0);
	//
//			mapBounds.dispose();
			
			return groundBody;
		}

		/**
		 * This is a helper function that makes calls that will attempt to extend
		 * one of the line segments already tracked by TiledMapHelper, if possible.
		 * The goal is to have as few line segments as possible.
		 * 
		 * Ex: If you have a line segment in the system that is from 1x1 to 3x3 and
		 * this function is called for a line that is 4x4 to 9x9, rather than add a
		 * whole new line segment to the list, the 1x1,3x3 line will be extended to
		 * 1x1,9x9. See also: LineSegment.extendIfPossible.
		 * 
		 * @param lsx1
		 *            starting x of the new line segment
		 * @param lsy1
		 *            starting y of the new line segment
		 * @param lsx2
		 *            ending x of the new line segment
		 * @param lsy2
		 *            ending y of the new line segment
		 * @param collisionLineSegments
		 *            the current list of line segments
		 */
		private void addOrExtendCollisionLineSegment(float lsx1, float lsy1, float lsx2, float lsy2, ArrayList<LineSegment> collisionLineSegments) {
			LineSegment line = new LineSegment(lsx1, lsy1, lsx2, lsy2);

			boolean didextend = false;

			for (LineSegment test : collisionLineSegments) {
				if (test.extendIfPossible(line)) {
					didextend = true;
					break;
				}
			}

			if (!didextend) {
				collisionLineSegments.add(line);
			}
		}



		/**
		 * Describes the start and end points of a line segment and contains a
		 * helper method useful for extending line segments.
		 */
		private class LineSegment {
			private Vector2 start = new Vector2();
			private Vector2 end = new Vector2();

			/**
			 * Construct a new LineSegment with the specified coordinates.
			 * 
			 * @param x1
			 * @param y1
			 * @param x2
			 * @param y2
			 */
			public LineSegment(float x1, float y1, float x2, float y2) {
				start = new Vector2(x1, y1);
				end = new Vector2(x2, y2);
			}

			/**
			 * The "start" of the line. Start and end are misnomers, this is just
			 * one end of the line.
			 * 
			 * @return Vector2
			 */
			public Vector2 start() {
				return start;
			}

			/**
			 * The "end" of the line. Start and end are misnomers, this is just one
			 * end of the line.
			 * 
			 * @return Vector2
			 */
			public Vector2 end() {
				return end;
			}

			/**
			 * Determine if the requested line could be tacked on to the end of this
			 * line with no kinks or gaps. If it can, the current LineSegment will
			 * be extended by the length of the passed LineSegment.
			 * 
			 * @param lineSegment
			 * @return boolean true if line was extended, false if not.
			 */
			public boolean extendIfPossible(LineSegment lineSegment) {
				/**
				 * First, let's see if the slopes of the two segments are the same.
				 */
				double slope1 = Math.atan2(end.y - start.y, end.x - start.x);
				double slope2 = Math.atan2(lineSegment.end.y - lineSegment.start.y, lineSegment.end.x - lineSegment.start.x);

				if (Math.abs(slope1 - slope2) > 1e-9) {
					return false;
				}

				/**
				 * Second, check if either end of this line segment is adjacent to
				 * the requested line segment. So, 1 pixel away up through sqrt(2)
				 * away.
				 * 
				 * Whichever two points are within the right range will be "merged"
				 * so that the two outer points will describe the line segment.
				 */
				if (start.dst(lineSegment.start) <= Math.sqrt(2) + 1e-9) {
					start.set(lineSegment.end);
					return true;
				} else if (end.dst(lineSegment.start) <= Math.sqrt(2) + 1e-9) {
					end.set(lineSegment.end);
					return true;
				} else if (end.dst(lineSegment.end) <= Math.sqrt(2) + 1e-9) {
					end.set(lineSegment.start);
					return true;
				} else if (start.dst(lineSegment.end) <= Math.sqrt(2) + 1e-9) {
					start.set(lineSegment.start);
					return true;
				}

				return false;
			}

			/**
			 * Returns a pretty description of the LineSegment.
			 * 
			 * @return String
			 */
			@Override
			public String toString() {
				return "[" + start.x + "x" + start.y + "] -> [" + end.x + "x" + end.y + "]";
			}
		}
		
	//=============================================================================
	
	@Override
	public void dispose() {
		if (mapRenderer != null) mapRenderer.dispose();
		if (shapeRenderer != null) shapeRenderer.dispose();
	}
}
