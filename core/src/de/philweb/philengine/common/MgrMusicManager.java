package de.philweb.philengine.common;

import com.badlogic.gdx.audio.Music;


public class MgrMusicManager {

	Game game;
	
	Music currentMusic;
	float fading_volume;
	boolean fadeOut = false;
	boolean fadeIn = false;
	final static float fadingVelocity = 0.02f;

	boolean isPlaying;
	
	float volumeMusic;
	float volumeSound;

	
	//=================================================================	
	
	public MgrMusicManager(Game game) {
		
		this.game = game;
		
		volumeMusic = game.getPreferences().getMusicVolume();
		volumeSound = game.getPreferences().getSoundVolume();
		
		isPlaying = false;		
	}

	//=================================================================
	
	
	public void update(float deltaTime) { 

		
		if (fadeOut == true) {
			
			fading_volume = fading_volume - (deltaTime * fadingVelocity);
			currentMusic.setVolume(fading_volume);

			if (fading_volume <= 0) {
				fadeOut = false;
				stop();
			}
		}
		if (fadeIn == true) {
			
			fading_volume = fading_volume + (deltaTime * fadingVelocity);
			currentMusic.setVolume(fading_volume);

			if (fading_volume >= volumeMusic) {
				currentMusic.setVolume(volumeMusic);
				fadeIn = false;
			}
		}
	}
	
	public void fadeOut() {
		
		fading_volume = volumeMusic;
		fadeOut = true;
	}

	public void fadeIn() {

		fadeIn = true;
	}
	
		
	
	//----------- controls -------------------------------------
	
	public void play() {
		
		if (game.getPreferences().isMusicEnabled() && currentMusic != null) {
			
			if (isPlaying == false) {
				
				currentMusic.setVolume(volumeMusic);
				currentMusic.play();		
				isPlaying = true;
//				Gdx.app.log("music", "isPlaying");
			}
			else {
				if (fadeOut == true) {
					
					fadeOut = false;
					fadeIn();
				}
			}
		}
	}
	
	
	
	public void pause() {
		
		if (isPlaying == true) {
			
			currentMusic.pause();
			isPlaying = false;
		}
	}
		
	
	public void stop() {
		
		if (isPlaying == true) {
		
			currentMusic.stop();			
			isPlaying = false;
//			Gdx.app.log("music", "stopped");
		}
	}
	
	
	
	//---- getters & setters ------------------------------------
	
	public boolean isPlaying() {
		return isPlaying;
	}
	
	

	
	public void setMusic(Music music) {
	
		if (this.currentMusic != music) {
			
			this.currentMusic = music;
			music.setLooping(true);
		}
	}

	

	
	public void setMusicVolume(float vol) {
		volumeMusic = vol;
		
		if (currentMusic != null) currentMusic.setVolume(volumeMusic);
	}
	
	public float getMusicVolume() {
		return volumeMusic;
	}
	
	public void setSoundVolume(float vol) {
		volumeSound = vol;
	}
	
	public float getSoundVolume() {
		return volumeSound;
	}
}
