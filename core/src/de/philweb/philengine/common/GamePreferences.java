
package de.philweb.philengine.common;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import de.philweb.philengine.apprater.IappRater;



public class GamePreferences implements IappRater {
    
	private Preferences prefs;
    private Game game;
    
    // constants
	
    public String PREFS_NAME; 
    public static final String PREF_MUSIC_ENABLED = "music.enabled";
    public static final String PREF_SOUND_ENABLED = "sound.enabled";
    public static final String PREF_PROGRESS_LEVEL = "progress.level";
    public static final String PREF_UNLOCKED_LEVEL = "unlock.level";
    
    //--------analytics-----------------
    public static final String PREF_ANALYTICS_OPTOUT = "PREF_ANALYTICS_OPTOUT";	
				  //--- 0: user is new and hasnt chosen opt in / out
				  //--- 1: returning user has opted out -> deactivate Google analytics
				  //--- 2: returning user has opted IN -> activate Google analytics
    
    public static final String PREF_CLIENTID = "PREF_CLIENTID";	// timestamp of first launch
    
    //---- language -----------------
    public static final String PREF_language = "PREF_language";
    
    public static final String PREF_askSafeZone = "PREF_askSafeZone";
    
    //----- for apprater ----------
    public static final String apprater_dontshowagain = "apprater_dontshowagain";
    public static final String apprater_launch_count = "apprater_launch_count";
    public static final String apprater_date_firstlaunch = "apprater_date_firstlaunch";
    
    //----- for options ----------
    public static final String options_points = "options_points";
    public static final String options_particles = "options_particles";
    public static final String options_lights = "options_lights";
    public static final String options_vibration = "options_vibration";
    public static final String options_OUYAsafezone = "options_OUYAsafezone";	// new - after refactoring
    public static final String PREF_LASTVERSION = "LASTVERSION";
    
    public static final String PREF_MUSIC_VOLUME = "PREF_MUSIC_VOLUME";
    public static final String PREF_SOUND_VOLUME = "PREF_SOUND_VOLUME";
    
    //----- for controllers/configs ----------
    public static final String lastUsedControllerPL1 = "lastUsedControllerPL1";
    public static final String lastUsedControllerPL2 = "lastUsedControllerPL2";
    public static final String lastUsedControllerPL3 = "lastUsedControllerPL3";
    public static final String lastUsedControllerPL4 = "lastUsedControllerPL4";
    //-----------------------------------------------------------------------

 
    public GamePreferences(Game game)    {
    
    	this.game = game;
  
    	PREFS_NAME = "philengine.prefs";
    	prefs = Gdx.app.getPreferences( PREFS_NAME );
    	
//    	arrayClickBounds = new ArrayList<Rectangle>();
//    	setClickBounds();
    }

   
    public String getPrefsName()    {
	    return prefs.getString( PREFS_NAME );
    }
    
    public Preferences getPreferences()    {
	    return prefs;
    }
        
    //----- EULA ------------------------------------
       
//    public String getEulaKeyFromPrefs()    {
//        return prefs.getString( EulaKey, "" );
//    }   
//    
//    public void setEulaKeyToPrefs(String key)    {
//		prefs.putString( EulaKey, key);
//		prefs.flush();
//	}
//    //----------------------------------------------------
    

    //----- get/set String to prefs ------------------------------------
    
    public String getStringFromPrefs(String key, String defaultValue)    {
        return prefs.getString(key, defaultValue);
    }   
    
    public void setStringToPrefs(String key, String value)    {
		prefs.putString(key, value);
		prefs.flush();
	}

    //----- get/set boolean to prefs ------------------------------------
    public boolean getBooleanFromPrefs(String key, boolean defaultValue)    {
        return prefs.getBoolean(key, defaultValue);
    }   
    
    public void setBooleanToPrefs(String key, boolean value)    {
		prefs.putBoolean(key, value);
		prefs.flush();
	}
    
    //----- get/set Integer to prefs ------------------------------------
    public int getIntegerFromPrefs(String key, int defaultValue)    {
        return prefs.getInteger(key, defaultValue);
    }   
    
    public void setIntegerToPrefs(String key, int value)    {
		prefs.putInteger(key, value);
		prefs.flush();
	}
    
    //----- PREF_askSafeZone (OUYA)  ------------------------------------
    //---- flag tells if rudemode asking dialog has already been shown
//    public boolean isAskSafeZoneSet()    {
//        return prefs.getBoolean( PREF_askSafeZone, false );
//    }   
//    
//    public void setAskSafeZone()    {
//		prefs.putBoolean( PREF_askSafeZone, true );
//		prefs.flush();
//	}
    //---------------------------------------------------
    
    
    public boolean isSoundEffectsEnabled()     {
        return prefs.getBoolean( PREF_SOUND_ENABLED, true );
    }

    public void setSoundEffectsEnabled(boolean soundEffectsEnabled )    {
    	prefs.putBoolean( PREF_SOUND_ENABLED, soundEffectsEnabled );
    	prefs.flush();
    }

    public void toggleSound()  {
		if (isSoundEffectsEnabled() == true) setSoundEffectsEnabled(false);
		else setSoundEffectsEnabled(true);
    }
    
    public boolean isMusicEnabled()  {
        return prefs.getBoolean( PREF_MUSIC_ENABLED, true );
    }

    public void setMusicEnabled( boolean musicEnabled )    {
    	prefs.putBoolean( PREF_MUSIC_ENABLED, musicEnabled );
    	prefs.flush();
    }

    
    public void toggleMusic()    {
		if (isMusicEnabled() == true) {
			
			setMusicEnabled(false);
			game.getMusicManager().stop();			
		}
		else {
			setMusicEnabled(true);
			game.getMusicManager().play();
		}
    }
    
   
    
    //------------ musik und sound --------------
    
    public float getMusicVolume()
    {
        return prefs.getFloat( PREF_MUSIC_VOLUME, 0.5f );
    }
    public void setMusicVolume(float volume )
    {
    	prefs.putFloat( PREF_MUSIC_VOLUME, volume );
    	prefs.flush();
    }
    public float getSoundVolume()
    {
        return prefs.getFloat( PREF_SOUND_VOLUME, 0.8f );
    }
    public void setSoundVolume(float volume )
    {
    	prefs.putFloat( PREF_SOUND_VOLUME, volume );
    	prefs.flush();
    } 
    
        
    //----- for apprater ----------
    
    @Override
    public int get_AppraterDontShowAgain()    {
	    return prefs.getInteger( apprater_dontshowagain, 0 );
    }
    
    @Override
    public void set_AppraterDontShowAgain(int dontshowagain)    {
    	prefs.putInteger( apprater_dontshowagain, dontshowagain );
    	prefs.flush();
    }
 
    @Override
    public long get_AppraterLaunchCount()    {
	    return prefs.getLong( apprater_launch_count, 0 );
    }
    
    @Override
    public void set_AppraterLaunchCount(long number)    {
    	prefs.putLong( apprater_launch_count, number );
    	prefs.flush();
    }
 
    @Override
    public long get_AppraterDateFirstLaunch()    {
	    return prefs.getLong( apprater_date_firstlaunch, 0 );
    }
    
    @Override
    public void set_AppraterDateFirstLaunch(long number)    {
    	prefs.putLong( apprater_date_firstlaunch, number );
    	prefs.flush();
    }
    //----------------------------------------
    
    public boolean isPointsEnabled()    {
        return prefs.getBoolean( options_points, true );
    }
    
    public boolean isParticlesEnabled()    {
        return prefs.getBoolean( options_particles, true );
//    	return false;
    }
    
    public boolean isLightsEnabled()     {
        return prefs.getBoolean( options_lights, true );
    }
    
    public boolean isVibrationEnabled()    {
        return prefs.getBoolean( options_vibration, true );
    }    
    //---------------------------------------------------------------
    
    
    // new method after refactoring 
    public boolean isOUYASafeZoneEnabled()    {
        
    	boolean safeZone = prefs.getBoolean( options_OUYAsafezone, false );
    	
    	if (game.getRunningDevice() == Game.ANDROID) safeZone = prefs.getBoolean( options_OUYAsafezone, false );
    	if (game.getRunningDevice() == Game.DESKTOP) safeZone = prefs.getBoolean( options_OUYAsafezone, false );
    	if (game.getRunningDevice() == Game.OUYA) safeZone = prefs.getBoolean( options_OUYAsafezone, true );
    	
    	return safeZone;
    }    
    
    
    public void checkSafeZone() {
    	
		if (isOUYASafeZoneEnabled() == true) game.getCameraManager().setCameraZoomAbsolute(game.OUYAsafeZoneCamZoom);
		else game.getCameraManager().setCameraZoomAbsolute(1f);
    }

    public void toggleSafeZone(boolean showToast)    {
		if (isOUYASafeZoneEnabled() == true) {
			disableSafeZone(showToast);
		}
		else {
			enableSafeZone(showToast);
		}
    }
    
    
    public void enableSafeZone(boolean showToast)    {
//		if (showToast == true) {
//			game.myRequestHandler.showToast(game.lang.getString("SafeZone has been enabled."));
//		}
		prefs.putBoolean( options_OUYAsafezone, true );
		prefs.flush();
		
		game.getCameraManager().setCameraZoomAbsolute(game.OUYAsafeZoneCamZoom);
    }
    
    public void disableSafeZone(boolean showToast)    {
//    	if (showToast == true) {
//    		game.myRequestHandler.showToast(game.lang.getString("SafeZone has been disabled."));
//    	}
		prefs.putBoolean( options_OUYAsafezone, false );
		prefs.flush();
		
		game.getCameraManager().setCameraZoomAbsolute(1f);
    }
    
    
    public void enablePoints()    {
//		bubblr.myRequestHandler.showToast(bubblr.lang.getString("Points have been enabled."));
		prefs.putBoolean( options_points, true );
		prefs.flush();
    }
    public void disablePoints()    {
//		bubblr.myRequestHandler.showToast(bubblr.lang.getString("Points have been disabled."));
		prefs.putBoolean( options_points, false );
		prefs.flush();
    }
    
  public void enableParticles()    {
//		bubblr.myRequestHandler.showToast(bubblr.lang.getString("Particle FX have been enabled."));
		prefs.putBoolean( options_particles, true );
		prefs.flush();
  }
  public void disableParticles()    {
//		bubblr.myRequestHandler.showToast(bubblr.lang.getString("Particle FX have been disabled."));
		prefs.putBoolean( options_particles, false );
		prefs.flush();
}
  
  public void enableLights()    {
//		bubblr.myRequestHandler.showToast(bubblr.lang.getString("Light FX have been enabled."));
		prefs.putBoolean( options_lights, true );
		prefs.flush();
  }
  public void disableLights()    {
//		bubblr.myRequestHandler.showToast(bubblr.lang.getString("Light FX have been disabled."));
		prefs.putBoolean( options_lights, false );
		prefs.flush();
  }
    
    public void toggleVibration()    {
		if (isVibrationEnabled() == true) {
//			bubblr.myRequestHandler.showToast(bubblr.lang.getString("Vibration has been disabled."));
			prefs.putBoolean( options_vibration, false );
		}
		else {
//			bubblr.myRequestHandler.showToast(bubblr.lang.getString("Vibration has been enabled."));
			prefs.putBoolean( options_vibration, true );
		}
		prefs.flush();
    }
    
    public void setVibration(boolean vib)    {

		prefs.putBoolean( options_vibration, vib );
		prefs.flush();
    }
    //-----------------------------------------------------    
    
    public void setLastUsedController(int player, String controllerDescr)    {

		if (player == 1) prefs.putString( lastUsedControllerPL1, controllerDescr );
		if (player == 2) prefs.putString( lastUsedControllerPL2, controllerDescr );
		if (player == 3) prefs.putString( lastUsedControllerPL3, controllerDescr );
		if (player == 4) prefs.putString( lastUsedControllerPL4, controllerDescr );
		prefs.flush();
    } 
    
    public String getLastUsedController(int player)    {
    	String retString = "";
    	
		if (player == 1) retString = prefs.getString( lastUsedControllerPL1, "" );
		if (player == 2) retString = prefs.getString( lastUsedControllerPL2, "" );
		if (player == 3) retString = prefs.getString( lastUsedControllerPL3, "" );
		if (player == 4) retString = prefs.getString( lastUsedControllerPL4, "" );
		
		return retString;
    } 
    
        
    public void setLastUsedConfigForController(int player, String controllerDescr, String configName)    {
    	
    	String key = "" + player + controllerDescr;
		prefs.putString( key, configName );
		prefs.flush();
    } 
        
    public String getLastUsedConfigForController(int player, String controllerDescr)    {

    	String key = "" + player + controllerDescr;
    	String value = prefs.getString( key, "" );

    	return value;
    }
    //-----------------------------------------------------
    
    
    //------- language --------------
    
    public String getLanguage()    {
	    return prefs.getString( PREF_language, ""); 
    }
    
    public void setLanguage(String lang)       {
    	prefs.putString( PREF_language, lang );
    	prefs.flush();
    }
    
    
    
}




