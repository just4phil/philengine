package de.philweb.philengine.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

public class PeSound {
	
	public Sound sound;
	float duration_ms;
	long start;
	long id;
	

	public PeSound(Sound soundFile, float duration_ms) {
		
		this.duration_ms = duration_ms;
		
		sound = soundFile;
	
		start = System.currentTimeMillis();			// maybe not neccessary
	}

	
	public PeSound(FileHandle datei, float duration_ms) {
		
		this.duration_ms = duration_ms;
		
		sound = Gdx.audio.newSound(datei);
	
		start = System.currentTimeMillis();			// maybe not neccessary	
	}
	
	
	
	public long play(float volume) {
		
		id = 0;
		
		if (System.currentTimeMillis() - start > duration_ms) {
			id = sound.play(volume);
			start = System.currentTimeMillis();
		}
		
		return id;
	}
}
