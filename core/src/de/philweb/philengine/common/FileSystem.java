package de.philweb.philengine.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class FileSystem {
	
    public static boolean isDirAvailable(String dir, boolean log) {
    	
    	FileHandle profileDataFile;
    	boolean isDirAvailable = false;
    	
		if (Gdx.files.isLocalStorageAvailable()) {
		
			if (log) Gdx.app.log("files", "Local storage available");
			if (log) Gdx.app.log("files", "Local storage path: " + Gdx.files.getLocalStoragePath().toString());
	
			profileDataFile = Gdx.files.local( dir );
			
			if (profileDataFile.exists()) {
				
				if (profileDataFile.isDirectory()) {
				
					if (log) Gdx.app.log(dir, "exists");
					isDirAvailable = true;
				}
				else {
					if (log) Gdx.app.log(dir, "name exists, but is NOT a directory!");
					
					profileDataFile.file().mkdirs();
					
					if (profileDataFile.exists() && profileDataFile.isDirectory()) {
						if (log) Gdx.app.log(dir, "has been succesfully created!");
						isDirAvailable = true;
					}
					else {
						if (log) Gdx.app.log(dir, "creation failed!");
					}
				}
			}
			else {
				if (log) Gdx.app.log(dir, "does NOT exist");
				
				profileDataFile.file().mkdirs();
				
				if (profileDataFile.exists() && profileDataFile.isDirectory()) {
					if (log) Gdx.app.log(dir, "has been succesfully created!");
					isDirAvailable = true;
				}
				else {
					if (log) Gdx.app.log(dir, "creation failed!");
				}
			}
			
		}
		else {
			
			if (log) Gdx.app.log("files", "Local storage is NOT available!");
		}
    	
    	return isDirAvailable;
    }
    
}
