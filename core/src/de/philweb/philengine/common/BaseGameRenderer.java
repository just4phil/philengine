package de.philweb.philengine.common;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

import de.philweb.philengine.gameobjects.BaseGameWorld;
import de.philweb.philengine.maps.MapService;
import de.philweb.philengine.particles2d.MgrParticleFxManager;

public abstract class BaseGameRenderer implements Disposable  {
	
	public Game game;
	public MapService map;
	public BaseGameWorld baseGameWorld;
	public SpriteBatch batch;
	public MgrParticleFxManager _particleFxManager;
	
	public BaseGameRenderer(Game game, MapService map, BaseGameWorld baseGameWorld) {
		this.game = game;
		this.map = map;
		this.baseGameWorld = baseGameWorld;
		this.batch = game.getSpritebatch();
		map.setGameRenderer(this);
	}
	
	public void setGameWorld(BaseGameWorld baseGameWorld) {
		this.baseGameWorld = baseGameWorld;
	}
	
	public BaseGameWorld getGameWorld() {
		return baseGameWorld;
	}
	
	public abstract void render();
	
	public void renderOnTopOfStage() { 	// optional: to draw on top of stage
	}
}
