
package de.philweb.philengine.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionWorld;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool.Poolable;

import de.philweb.philengine.common.Game;

public class GoGameObject3d extends ModelInstance implements Poolable, Disposable { // extends ModelInstance

	public Game game;
	public ModelInstance modelInstance;

	protected int objectID;
	protected float width_m; // in meters
	protected float height_m; // in meters
	protected float depth_m; // in meters

	boolean useBullet = false;

	Quaternion quaternion;

	float rotationX;
	float rotationY;
	float rotationZ;

	protected static Quaternion q = new Quaternion();
	protected static Matrix4 mtx = new Matrix4();

	/** height, width, depth dimensions of the entity */
	protected Vector3 dimen = new Vector3();
	protected Quaternion rotation = new Quaternion();
	private Vector3 tmp = new Vector3();

	// ----- for bullet ---------
	btCollisionShape bulletCollisionShape;
	public btCollisionObject body;
	btCollisionWorld bulletCollisionWorld;

	public GoGameObject3d (Game game, String modelFile, int objectID, float width_m, float height_m, float depth_m, float x,
		float y, float z) {
		super(game.getCameraManager().getModelLoader().loadModel(Gdx.files.internal(modelFile)));
		initialize(game, objectID, width_m, height_m, depth_m, x, y, z);
	}

	public GoGameObject3d (Game game, final Material material, final long attributes, int objectID, float width_m, float height_m,
		float depth_m, float x, float y, float z) {
		super(game.getCameraManager().getModelBuilder().createBox(width_m, height_m, depth_m, material, attributes));
		initialize(game, objectID, width_m, height_m, depth_m, x, y, z);
	}

	public void initialize (Game game, int objectID, float width_m, float height_m, float depth_m, float x, float y, float z) {
		this.game = game;
		this.objectID = objectID;
		this.width_m = width_m;
		this.height_m = height_m;
		this.depth_m = depth_m;
		quaternion = new Quaternion();

		setPosition(x, y, z);
		updateTransforms();
	}

	public void initializeBulletCollision (int userDataIntID, btCollisionWorld bulletCollisionWorld, btCollisionShape shape,
		short collisionFilterGroup, short collisionFilterMask) {
		if (useBullet == false) {
			this.bulletCollisionWorld = bulletCollisionWorld;
			useBullet = true;
			bulletCollisionShape = shape;
			body = new btCollisionObject();
			body.setCollisionShape(shape);
			body.setWorldTransform(transform);
			bulletCollisionWorld.addCollisionObject(body, collisionFilterGroup, collisionFilterMask);
			body.setUserValue(userDataIntID); // integer position in instances array of gameworld
			body.setCollisionFlags(body.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
		}
	}

	public void hit (int reduceEnergy) {
	}

	public int getObjectID () {
		return objectID;
	}

	public Vector3 getPosition () {
		transform.getTranslation(tmp);
		return tmp;
	}

	public float getWidth () {
		return width_m;
	}

	public float getHeight () {
		return height_m;
	}

	public float getDepth () {
		return depth_m;
	}

	@Override
	public void reset () {

	}

	// ================================================

// // TODO check this works properly
// public void faceTowards(Vector3 targ) {
// float desired = Tools.getAngleFromAtoB(movement.getPosition(), targ, Vector3.Y);
// rotation.setEulerAngles(-desired, rotation.getPitch(), rotation.getRoll());
// }

	public void updateTransforms () {
		// physics body transform
		q.setEulerAngles(rotation.getYaw(), rotation.getPitch(), rotation.getRoll());
		mtx.set(q);
		mtx.setTranslation(getPosition());
		transform.set(mtx);

		if (useBullet) body.setWorldTransform(mtx);
	}

// public void setDestination(Vector3 d) {
// movement.setDestination(d);
// }
//
// public void setRelativeDestination(Vector3 delta) {
// tmp.set(movement.getPosition()).add(delta);
// setDestination(tmp);
// }
//
// /** Stops ground units from moving up when looking up.
// * for example, if an entity is rotated to be facing almost straight up,
// * this method relativizes the destination to be "in front of" the entity
// * on the xz (ground) plane */
// public void setRelativeDestinationByYaw(Vector3 delta) {
// // TODO what happens if one of the added vectors == Vector3.Y? i.e. straight up or straight down
// relativizeByYaw(delta);
// tmp.set(movement.getPosition()).add(delta);
// setDestination(tmp);
// }

	/** transform vector based on the current rotation, but set pitch to zero, useful for relative directions like "forward" and
	 * "back" */
	public Vector3 relativizeByYaw (Vector3 v) {
		q.setEulerAngles(rotation.getYaw(), rotation.getPitch(), rotation.getRoll());
		q.transform(v);
		return v;
	}

	/** transform vector by current rotation, makes vector relative to current facing */
	public Vector3 relativize (Vector3 v) {
		rotation.transform(v);
		return v;
	}

// public void setPosition(Vector3 pos) {
// if (pos == null) throw new NullPointerException();
// if (movement == null) throw new NullPointerException();
// movement.setPosition(pos);
// }

	public void setPosition (float x, float y, float z) {
		transform.setToTranslation(x, y, z);
	}

	public void addToPosition (float x, float y, float z) {
		transform.setToTranslation(getPosition().x + x, getPosition().y + y, getPosition().z + z);
	}

// public void adjustPosition(Vector3 delta) {
// movement.getPosition().add(delta);
// }
//
// public void setVelocity(Vector3 vel) {
// movement.getVelocity().set(vel);
// }
//
// public Movement getMovement() {
// return movement;
// }

// public Vector3 getPosition() {
// return movement.getPosition();
// }
//
// public Vector3 getVelocity() {
// return movement.getVelocity();
// }
//
// public void adjustVelocity(Vector3 delta) {
// movement.getVelocity().add(delta);
// }

	public void setRotation (Quaternion newRot) {
		rotation.set(newRot);
	}

// public void adjustRotation(Direction.Rotation rot) {
// System.out.println("adjust rotation: " + rot.vector);
// float rotSpeed = 4f;
// adjustYaw(-rot.vector.x * rotSpeed);
// adjustPitch(-rot.vector.y * rotSpeed);
// adjustRoll(-rot.vector.z * rotSpeed);
// }

	public Quaternion getRotation () {
		return rotation;
	}

// /** adjust velocity based on relative directions. i.e. Vector3.Z == forward, (0, 1, 1) == forward-up */
// public void adjustVelocityRelativeByYaw(Vector3 delta) {
// adjustVelocity(relativizeByYaw(delta));
// }

	public void setYawPitchRoll (float y, float p, float r) {
		getRotation().setEulerAngles(y, p, r);
	}

	public void setYawPitchRoll (Vector3 rot) {
		getRotation().setEulerAngles(rot.x, rot.y, rot.z);
	}

// public void lookAt(Vector3 pos) {
// tmp.set(pos).sub(getPosition());
// q.setFromCross(Vector3.Z, tmp.nor());
// setYawPitchRoll(q.getYaw(), getPitch(), getRoll());
// }

	public float getYaw () {
		return rotation.getYaw();
	}

	public void setYaw (float amt) {
		rotation.setEulerAngles(amt, rotation.getPitch(), rotation.getRoll());
	}

	public void adjustYaw (float amt) {
		float yaw = getYaw();
		yaw += amt;
		setYaw(yaw);
	}

	public float getPitch () {
		return rotation.getPitch();
	}

	public void setPitch (float amt) {
		rotation.setEulerAngles(rotation.getYaw(), amt, rotation.getRoll());
	}

	public void adjustPitch (float amt) {
		float pitch = getPitch();
		// avoid gimbal lock
		// technically could use Quaternions for free rotation, but not necessary for FPS
		pitch = MathUtils.clamp(pitch + amt, -89f, 89f);
		setPitch(pitch);
	}

	public float getRoll () {
		return rotation.getRoll();
	}

	public void setRoll (float amt) {
		rotation.setEulerAngles(rotation.getYaw(), rotation.getPitch(), amt);
	}

	public void adjustRoll (float amt) {
		float roll = getRoll();
		roll += amt;
		setRoll(roll);
	}

	// ----------------------

	@Override
	public void dispose () {
		if (useBullet) {
			bulletCollisionShape.dispose();
			body.dispose();
// model.dispose();
		}
	}

}
