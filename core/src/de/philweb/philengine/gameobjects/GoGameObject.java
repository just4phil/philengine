package de.philweb.philengine.gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool.Poolable;

import de.philweb.philengine.common.Game;
import de.philweb.philengine.maps.MapService;
import de.philweb.philengine.spawnsystem.Ispawnable;


public class GoGameObject implements Poolable, Ispawnable {
		
	public Game game;
	public MapService map;
	protected int objectID;
	protected Body body = null;
	protected float width_m;			// in meters
	protected float height_m;			// in meters
	protected Vector2 position_m;		// in meters	// for dynamic objects this will be the interpolated position!
	protected TextureRegion region = null;
	protected String assetName;
	
	public GoGameObject() {	}
		
	public GoGameObject(MapService map, int objectID, float width_m, float height_m, float x, float y) {
		initialize(map, objectID, width_m, height_m, x, y);
	}
	
	public void initialize(MapService map, int objectID, float width_m, float height_m, float x, float y) {
		this.game 		= map.game;
		this.map		= map;
		this.objectID 	= objectID;
		this.width_m 	= width_m;
		this.height_m 	= height_m;
		position_m 		= new Vector2(x, y);
	}
	
	public Body addBox2dBody(World box2dworld, BodyType bodyType, float angleInDegrees) {
		
		//---- generate box2d body ---------
		BodyDef characterBodyDef = new BodyDef();
		characterBodyDef.type = bodyType;
		characterBodyDef.position.set(position_m.x, position_m.y);
		characterBodyDef.angle = MathUtils.degreesToRadians * angleInDegrees;
		body = box2dworld.createBody(characterBodyDef);
		body.setFixedRotation(false);
		body.setUserData(this);
		body.setTransform(position_m.x, position_m.y, MathUtils.degreesToRadians * angleInDegrees);
		
//		Gdx.app.log("MathUtils.degreesToRadians * angleInDegrees: " + MathUtils.degreesToRadians * angleInDegrees,
//				" // getAngle: " + body.getAngle());
		
		// all dynamich box2d objects should be in the interpolation array of the gameobjectmanager.... but this must be handled in the game to avoid dependencies
//		if (bodyType == BodyDef.BodyType.DynamicBody || bodyType == BodyDef.BodyType.KinematicBody) {	}
		
		return body;
	}
		
	public void render(SpriteBatch batch) {
		if (region != null) batch.draw(region, getPosition().x, getPosition().y, getWidth(), getHeight());
	}
	
	public GoGameObject setTextureRegion(TextureRegion region) {
		this.region = region;
		return this;
	}
	
	//FIXME: QUATSCH!! hier muss fuer eine spezielle FIX der Filter gesetzt werden!!!!
//	public void setBox2dFilter(int userDataFixID, Filter maskFilter) {
//		Array<Fixture> fixes = new Array<Fixture>();
//		fixes = getBody().getFixtureList();
//		for (int i = 0; i < fixes.size; i++) {		
//			fixes.get(i).setUserData(userDataFixID);	
//			fixes.get(i).setFilterData(maskFilter);				
//		}
//	}
	
	public void setBox2dFilter(Fixture fix, Filter maskFilter) {
		if (fix != null) fix.setFilterData(maskFilter);	
	}
	
	public void setBox2dFilter(int userDataFixID, Filter maskFilter) {
		Fixture fix = getBox2dFixture(userDataFixID);
		if (fix != null) fix.setFilterData(maskFilter);	
	}
	
	public Fixture getBox2dFixture(int userDataFixID) {
		Fixture returnFix = null;
		Array<Fixture> fixes = new Array<Fixture>();
		fixes = getBody().getFixtureList();
		for (int i = 0; i < fixes.size; i++) {	
			Object obj = fixes.get(i).getUserData();
			if (obj != null) {
				int fixID = (Integer)obj;
				if (fixID == userDataFixID) {
					returnFix = fixes.get(i);
					break;
				}
			} 
		}
		return returnFix;
	}
	
	public void hit(int reduceEnergy) {
	}
	
	public int getObjectID() {
		return objectID;
	}
	// will be used by power-ups that must get a type
	public void setObjectID(int id) {
		objectID = id;
	}
	public Body getBody() {
		return body;
	}
	public Vector2 getPosition() {
		return position_m;
	}
	public GoGameObject setPosition(float x, float y) {
		position_m.x = x;
		position_m.y = y;
		if (body != null) body.setTransform(position_m.x, position_m.y, 0f);
		return this;
	}
	public GoGameObject setPosition(float x, float y, float rotationInRadians) {
		position_m.x = x;
		position_m.y = y;
		if (body != null) body.setTransform(position_m.x, position_m.y, rotationInRadians);
		return this;
	}
	public float getWidth() {
		return width_m;
	}
	public float getHeight() {
		return height_m;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public String getAssetName() {
		return assetName;
	}
	
	@Override
	public void reset() {
	}
	
	@Override
	public void setSpawningPosition (float x, float y, float z, float rotationInRadians) {
		setPosition(x, y, rotationInRadians);
	}

	public void startParticleFxSpawn(float x, float y) {	}
	
	//--- after spawning
	@Override
	public void setActive () {
		if (body != null) body.setActive(true);
	}

	@Override
	public void doAfterSpawning() {	}
}