//package de.philweb.philengine.gameobjects;
//
//import java.io.IOException;
//
//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
//import com.badlogic.gdx.graphics.g2d.Sprite;
//import com.badlogic.gdx.utils.Pool.Poolable;
//
//
//public class PeParticleEmitter extends ParticleEmitter implements Poolable {
//
//	int ID;
//	float scale;	// needed to restore original scale after pool.free
//	boolean restoreScaleAfterFX = false;
//	
//	public PeParticleEmitter(int ID, String file, Sprite sprite, float x, float y) {
//		initialize(ID, file, sprite, x, y, 0.03f);
//	}
//	
//	public PeParticleEmitter(int ID, String file, Sprite sprite, float x, float y, float scale) {
//		initialize(ID, file, sprite, x, y, scale);
//	}
//	
//	public PeParticleEmitter(int ID, String file, Sprite sprite, GoGameObject gameObject) {
//		initialize(ID, file, sprite, gameObject.position_m.x, gameObject.position_m.y, 0.03f);
//	}
//	
//	public PeParticleEmitter(int ID, String file, Sprite sprite, GoGameObject gameObject, float scale) {
//		initialize(ID, file, sprite, gameObject.position_m.x, gameObject.position_m.y, scale);
//	}
//	
//	void initialize(int ID, String file, Sprite sprite, float x, float y, float scale) {
//		this.ID = ID;
//			
//		try {
//			load(Gdx.files.internal(file).reader(2024));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		setSprite(sprite);
//		scaleEffect(scale, false);
//		setPosition(x, y);
//		setAdditive(false);	// performance-optim.: disable automatic blend function of .draw!
//	}
//	
//	public int getID() {
//		return ID;
//	}
//		
//	public float getScalefactor() {
//		return scale;
//	}
//	
//	//---- called by pool.free
//	@Override
//	public void reset() {
////		reset();
//		if (restoreScaleAfterFX) scaleEffect(1f/scale, false);	// scale back to original if scaling will be applied multiple times
//	}
//	
//   public void scaleEffect(float scaleFactor, boolean restoreScaleAfterFX){
//	      		         
//	   this.scale = scaleFactor; // save scale for restore later
//	   this.restoreScaleAfterFX = restoreScaleAfterFX;
//	   
//	         getScale().setHigh(getScale().getHighMin()*scaleFactor, getScale().getHighMax()*scaleFactor);
//	         getScale().setLow(getScale().getLowMin()*scaleFactor, getScale().getLowMax()*scaleFactor);
//	         
//	         getVelocity().setHigh(getVelocity().getHighMin()*scaleFactor, getVelocity().getHighMax()*scaleFactor);
//	         getVelocity().setLow(getVelocity().getLowMin()*scaleFactor, getVelocity().getLowMax()*scaleFactor);
//	         
//	         getGravity().setHigh(getGravity().getHighMin()*scaleFactor, getGravity().getHighMax()*scaleFactor);
//	         getGravity().setLow(getGravity().getLowMin()*scaleFactor, getGravity().getLowMax()*scaleFactor);
//	         
//	         getWind().setHigh(getWind().getHighMin()*scaleFactor, getWind().getHighMax()*scaleFactor);
//	         getWind().setLow(getWind().getLowMin()*scaleFactor, getWind().getLowMax()*scaleFactor);
//	         
//	         getSpawnWidth().setHigh(getSpawnWidth().getHighMin()*scaleFactor, getSpawnWidth().getHighMax()*scaleFactor);
//	         getSpawnWidth().setLow(getSpawnWidth().getLowMin()*scaleFactor, getSpawnWidth().getLowMax()*scaleFactor);
//	         
//	         getSpawnHeight().setHigh(getSpawnHeight().getHighMin()*scaleFactor, getSpawnHeight().getHighMax()*scaleFactor);
//	         getSpawnHeight().setLow(getSpawnHeight().getLowMin()*scaleFactor, getSpawnHeight().getLowMax()*scaleFactor);
//	         
//	         getXOffsetValue().setLow(getXOffsetValue().getLowMin()*scaleFactor, getXOffsetValue().getLowMax()*scaleFactor);
//	         getYOffsetValue().setLow(getYOffsetValue().getLowMin()*scaleFactor, getYOffsetValue().getLowMax()*scaleFactor);
//	  }
//}
