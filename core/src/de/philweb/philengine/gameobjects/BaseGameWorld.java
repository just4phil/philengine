package de.philweb.philengine.gameobjects;

import com.badlogic.gdx.utils.Disposable;

import de.philweb.philengine.common.Game;
import de.philweb.philengine.maps.MapService;

public abstract class BaseGameWorld implements Disposable {

	public Game game;
	public MapService map;
	public BaseGameMode baseGameMode;		// for gameMode-specific code

	public BaseGameWorld(Game game) {
		this(game, null);
	}
	
	public BaseGameWorld(Game game, MapService map) {
		this.game = game;
		if (map != null) setMapService(map);
		setGameMode(new BaseGameModeSTUB(game, map));
	}
	
	public MapService getMap() {
		return map;
	}

	public void setMapService(MapService map) {
		this.map = map;
		map.setGameWorld(this);
	}
	
	public void setGameMode(BaseGameMode baseGameMode) {
		this.baseGameMode = baseGameMode;
	}
	
	public BaseGameMode getGameMode() {
		return baseGameMode;
	}
	
	public abstract void update(float deltaTime);
}
