package de.philweb.philengine.gameobjects;

import java.util.ArrayList;



public class MgrGameObjectManager {

	private ArrayList<GoDynamicGameObject> dynamicGameObjectList;
	int i;
	int size;
	GoDynamicGameObject dynamicGameObject;
	
	
	public MgrGameObjectManager() {
	
		dynamicGameObjectList = new ArrayList<GoDynamicGameObject>(100);
	}
	
		
	
	public void copyCurrentPosition() {
		
		size = dynamicGameObjectList.size();
		
		for (i = 0; i < size; i++) {
		
			dynamicGameObject = dynamicGameObjectList.get(i);
			
			if (dynamicGameObject.body != null) {
				
//				if (dynamicGameObject.body.getType() == BodyDef.BodyType.DynamicBody && dynamicGameObject.body.isActive() == true) {
				if (dynamicGameObject.body.isActive() == true) {
					
					dynamicGameObject.position_previous.x = dynamicGameObject.body.getPosition().x;
					dynamicGameObject.position_previous.y = dynamicGameObject.body.getPosition().y;
					dynamicGameObject.angle_previous = dynamicGameObject.body.getAngle();
				}
			}
		}
	}
	
	
	
	public void interpolateCurrentPosition(float alpha) {
		
		size = dynamicGameObjectList.size();
		
		for (i = 0; i < size; i++) {
		
			dynamicGameObject = dynamicGameObjectList.get(i);
			
			if (dynamicGameObject.body != null) {
				
//				if (dynamicGameObject.body.getType() == BodyDef.BodyType.DynamicBody && dynamicGameObject.body.isActive() == true) {
				if (dynamicGameObject.body.isActive() == true) {

					//---- interpolate: currentState*alpha + previousState * ( 1.0 - alpha ); ------------------
					dynamicGameObject.position_m.x = dynamicGameObject.body.getPosition().x * alpha + dynamicGameObject.position_previous.x * (1.0f - alpha);
					dynamicGameObject.position_m.y = dynamicGameObject.body.getPosition().y * alpha + dynamicGameObject.position_previous.y * (1.0f - alpha);
					dynamicGameObject.angle = dynamicGameObject.body.getAngle() * alpha + dynamicGameObject.angle_previous * (1.0f - alpha);
				}
			}
		}
	}
	
	
	
	public ArrayList<GoDynamicGameObject> getDynamicGameObjectList() {
		
		return dynamicGameObjectList;
	}
	
	public void addDynamicGameObject(GoDynamicGameObject dynamicGameObject) {
		
//		dynamicGameObject.position_m.x = dynamicGameObject.body.getPosition().x;
//		dynamicGameObject.position_m.y = dynamicGameObject.body.getPosition().y;
//		dynamicGameObject.position_previous.x = dynamicGameObject.position_m.x;
//		dynamicGameObject.position_previous.y = dynamicGameObject.position_m.y;
		
		if (dynamicGameObject.isInInterpolationArray == false) {	// dont save an object more than one time
			
			dynamicGameObject.updatePositions();
			dynamicGameObject.isInInterpolationArray = true; 
			dynamicGameObjectList.add(dynamicGameObject);
		}
	}
	
	public void removeDynamicGameObject(GoDynamicGameObject dynamicGameObject) {
		
		if (dynamicGameObject.isInInterpolationArray == true) {		// avoid nullpointer
		
			dynamicGameObject.isInInterpolationArray = false; 
			dynamicGameObjectList.remove(dynamicGameObject);
		}
	}
	
	public void clearList() {
		
		for (i = 0; i < dynamicGameObjectList.size(); i++) {
			dynamicGameObjectList.get(i).isInInterpolationArray = false; 
		}
		
		dynamicGameObjectList.clear();
	}
}
