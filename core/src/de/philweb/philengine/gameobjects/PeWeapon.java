package de.philweb.philengine.gameobjects;

import com.badlogic.gdx.physics.box2d.Body;

public interface PeWeapon {
	
	public abstract void update(float deltaTime);
	
	public abstract void useWeapon();
	
	public abstract void dropWeapon();
	
	public abstract float getOwnAngle();	// this is NOT the box2d angle
	
	public int getWeaponID();

	public abstract Body getBody();
	
	public void setReloadTime(long reloadTime);
}
