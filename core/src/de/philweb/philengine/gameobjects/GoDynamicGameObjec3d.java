package de.philweb.philengine.gameobjects;

import com.badlogic.gdx.graphics.g3d.Material;

import de.philweb.philengine.common.Game;
import de.philweb.philengine.controllers.AbstractController;


public class GoDynamicGameObjec3d extends GoGameObject3d {
	
	protected AbstractController controller = null;
	
	boolean isConstantlyMoving = false;
	float velocityX;
	float velocityY;
	float velocityZ;
	
	boolean isConstantlyRotating = false;
	float rotationVelocityX;
	float rotationVelocityY;
	float rotationVelocityZ;
	
	
	public GoDynamicGameObjec3d(Game game, String modelFile, int objectID, float width_m, float height_m, float depth_m, float x, float y, float z, float angleInDegrees) {
		super(game, modelFile, objectID, width_m, height_m, depth_m, x, y, z);
	}
	
	public GoDynamicGameObjec3d(Game game, final Material material, final long attributes, int objectID, float width_m, float height_m, float depth_m, float x, float y, float z, float angleInDegrees) {
		super(game, material, attributes, objectID, width_m, height_m, depth_m, x, y, z);
	}
	
	public void update (float deltatime) {
		
		if (isConstantlyRotating) calculateConstantRotation(deltatime);
		if (isConstantlyMoving) addToPosition(velocityX * deltatime, velocityY * deltatime, velocityZ * deltatime);		
		
//		Gdx.app.log("x: " + position_m.x, "// y: " + position_m.y + "  // z: " + position_m.z);
		
		updateTransforms();
	}
		
	public void initializeConstantVelocity(float velocityX, float velocityY, float velocityZ) {
		this.velocityX = velocityX;
		this.velocityY = velocityY;
		this.velocityZ = velocityZ;
		isConstantlyMoving = true;
	}
	
	public void stopConstantMovement() {
		isConstantlyMoving = false;
	}
	
	public void initializeConstantRotation(float rotationVelocityX, float rotationVelocityY, float rotationVelocityZ) {
		this.rotationVelocityX = rotationVelocityX;
		this.rotationVelocityY = rotationVelocityY;
		this.rotationVelocityZ = rotationVelocityZ;
		isConstantlyRotating = true;
	}
	public void stopConstantRotation() {
		isConstantlyRotating = false;
	}
	
	private void calculateConstantRotation(float deltatime) {
//		modelInstance.transform.setToTranslation(position_m);
//		modelInstance.transform.trn(position_m);
		
//		transform.getRotation(quaternion);
		
//		rotationX = quaternion.getAxisAngle(Vector3.X) + (deltatime * rotationVelocityX * 5);       
//		rotationX = rotationX + (deltatime * rotationVelocityX * 5);
//		Gdx.app.log("quaternion.getAxisAngle(Vector3.X)" + quaternion.getAxisAngle(Vector3.X), "rotationX: " + rotationX);
//		if (rotationX > 360) rotationX = rotationX - 360;
//		if (rotationX < 0) rotationX = 360 + rotationX;
//		
//		rotationY = quaternion.getAxisAngle(Vector3.Y) + (deltatime * rotationVelocityY * 5);
//		if (rotationY > 360) rotationY = rotationY - 360;
//		if (rotationY < 0) rotationY = 360 + rotationY;
//		
//		rotationZ = quaternion.getAxisAngle(Vector3.Z) + (deltatime * rotationVelocityZ * 5);	// TODO fake----
//		if (rotationZ > 360) rotationZ = rotationZ - 360;
//		if (rotationZ < 0) rotationZ = 360 + rotationZ;
		
//		quaternion.setFromAxis(Vector3.X, rotationX).setFromAxis(Vector3.Y, rotationY).setFromAxis(Vector3.Z, rotationZ);
		
		adjustYaw(deltatime * rotationVelocityY * 5);
		adjustPitch(deltatime * rotationVelocityX * 5);
		adjustRoll(deltatime * rotationVelocityZ * 5);
		
//		Gdx.app.log("getYaw()" + getYaw(), "");
	}
	
	
	public void setController(AbstractController controller) {
		this.controller = controller;
//		this.controller.setGameObject(this); 		// sets the owner gameobject in the controller instance
	}
	public AbstractController getController() {
		return controller;
	}
}
