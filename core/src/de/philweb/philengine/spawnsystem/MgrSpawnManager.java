package de.philweb.philengine.spawnsystem;

/** SpawnManager for the spawnsystem.
* manages all spawntasks
* 
* @author just4phil - heavily.loaded.games@googlemail.com */

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;

import de.philweb.philengine.spawnsystem.SpawnTask.SpawnState;

public class MgrSpawnManager {

	private ArrayList<SpawnTask<? extends Ispawnable>> spawnTasksList;
	int i;
	int size;
	protected long now;
	public long accumulatedPauseTime;
	
	public MgrSpawnManager() {
		spawnTasksList = new ArrayList<SpawnTask<? extends Ispawnable>>(5);
	}
	
	public void reset() {
		accumulatedPauseTime = 0L;
		spawnTasksList.clear();
	}
	
	public void addSpawnTask(SpawnTask<? extends Ispawnable> spawnTask) {
		if (spawnTask.verify() == true) {
			spawnTask.setSpawnManager(this);
			spawnTask.setSpawnState(SpawnState.RUNNING);
			spawnTask.startTime = System.nanoTime();
			spawnTasksList.add(spawnTask);
		}
		else Gdx.app.log("addSpawnTask", "spawnTask ist not valid!");
	}
	
	public boolean spawnTaskExists(int ID) {
		for (i = 0; i < spawnTasksList.size(); i++) {
			if (spawnTasksList.get(i).getID() == ID) return true;
		}
		return false;
	}
	
	public SpawnTask<? extends Ispawnable> getSpawnTask(int ID) {
		SpawnTask<? extends Ispawnable> retunTask = null;
		SpawnTask<? extends Ispawnable> task;
		for (i = 0; i < spawnTasksList.size(); i++) {
			task = spawnTasksList.get(i);
			if (task.getID() == ID) retunTask = task;
		}
		return retunTask;
	}
	
//	public void pauseSpawnTask(int ID) {
//		getSpawnTask(ID).pause();
//	}
//	
//	public void restartSpawnTask(int ID) {
//		getSpawnTask(ID).restart();
//	}
//	
//	public void finishSpawnTask(int ID) {
//		getSpawnTask(ID).finish();
//	}
	
	public void update(float deltatime) {

		now = System.nanoTime();

		size = spawnTasksList.size();		
		for (i = 0; i < size; i++) {
			SpawnTask<? extends Ispawnable> task = spawnTasksList.get(i);
			
			switch (task.getSpawnState()) {
						
				case RUNNING:
					if (accumulatedPauseTime > 0) task.addPauseTime(accumulatedPauseTime);	// add paused time deltas
					task.update(deltatime);
					break;
									
				case FINISHED:
					task.reset(); // Poolable
					spawnTasksList.remove(i);
					size = spawnTasksList.size();	// TODO: checken ob dabei elemente uebersprungen werden!!-------
					break;
					
				case NEW:
					break;
					
				case PAUSED:
					//---- check if DestinationArraySize is below max size and restart spawntask
					if (task.maxArraySize > SpawnTask.ARRAY_SIZE_UNLIMITED) {	// first check if maxSize is defined
						if (task.getDestinationArraySize() < task.maxArraySize) {
							task.restart();
//							Gdx.app.log("SPAWNTASK", "restarted");
						}
					}
					break;
					
				default:
					break;
			}
		}
		if (accumulatedPauseTime > 0) accumulatedPauseTime = 0;
	}
	
	public int getSpanTasksCount() {
		return spawnTasksList.size();
	}
	
	//---- used for gamestate ready / paused
	public void updatePAUSE(float deltatime) {
		accumulatedPauseTime = accumulatedPauseTime + (long)(deltatime * 1000000000L);
//		Gdx.app.log("accumulatedPauseTime", "" + accumulatedPauseTime);
	}
	
//	private void removeSpawnTask(int ID) {
//	for (i = 0; i < spawnTasksList.size(); i++) {
//		if (spawnTasksList.get(i).ID == ID) {
//			spawnTasksList.remove(i);
//			break;
//		}
//	}
//}
}
