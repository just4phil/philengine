package de.philweb.philengine.spawnsystem;

import com.badlogic.gdx.Gdx;

public class QuotaElement {

	private int ID;
	private int priority;
	private float quota;
	private QuotaSpawnTask<? extends Ispawnable> quotaSpawnTask;
	private boolean isActive;

	public QuotaElement() {
		initialize(-1 , 0);
	}

	public QuotaElement(int ID, int priority) {
		initialize(ID, priority);
	}
	
	public QuotaElement(int ID, int priority, boolean isActive) {
		initialize(ID, priority);
		setActive(isActive);
	}

	// dont initialize after you added to a quotaspawntask!
	public void initialize(int ID, int priority) {
		if (quotaSpawnTask == null) {
			this.ID = ID;
			this.priority = priority;
			this.quota = 0f;
			isActive = true;
		}
		else Gdx.app.log("Error", "QuotaElement - initialize: quotaSpawnTask != null. You tried to initialize an element that is currently added to a spawntask!");
	}

	public int getID() {
		return ID;
	}
	public int getPriority() {
		return priority ;
	}
	public float getQuota() {
		return quota;
	}
	
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	public void setPriority(int priority) {
		this.priority = priority;
		if (quotaSpawnTask != null) quotaSpawnTask.updateQuotas(); 	// if already added to a quotaspawntask, call  quotaspawntask.updateQuotas()
	}
	
	// should only be used be QuotaSpawnTask to calculate and set the quota depending on priority
	protected void setQuota(float quota) {
		this.quota = quota;
	}

	protected void setQuotaSpawnTask(QuotaSpawnTask<? extends Ispawnable> quotaSpawnTask) {
		this.quotaSpawnTask = quotaSpawnTask;
	}
}
