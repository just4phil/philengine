package de.philweb.philengine.spawnsystem;

 /** Callback class for the spawnsystem.
 * defines the hook method to be notified of a spawn
 * 
 * @author just4phil - heavily.loaded.games@googlemail.com */

public interface IspawnCallback<T extends Ispawnable> {

	public void reportSpawnedObject (T gameObject);
}
