package de.philweb.philengine.spawnsystem;

/** SpawnTask for the spawnsystem.
* manages all a concrete spawntask
* 
* @author just4phil - heavily.loaded.games@googlemail.com */

import java.util.ArrayList;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pool.Poolable;

public class SpawnTask<T extends Ispawnable> implements Poolable {

	public final static int ARRAY_SIZE_UNLIMITED = -1;	//--- default: -1 = unlimited
	
	/** Defines possible states for a {@link SpawnState}. */
	public enum SpawnState {
		NEW,
		RUNNING,
		PAUSED,
		FINISHED
	}
	/** Defines possible modes for a {@link SpawnMode}. */
	public enum SpawnMode {
		UNDEFINED,
		ONCE,
		CONTINOUS
	}
	/** Defines possible modes for a {@link ValueMode}. */
	public enum ValueMode {
		UNDEFINED,
		FIX,
		RANDOM,
		EMPTY		// used for only-callback-tasks
	}
	MgrSpawnManager spawnMgr;
	protected int ID;	
	protected Pool<T> sourcePool; 
	protected ArrayList<T> destinationArray;
	protected T singleObjectToSpawn;
	protected IspawnCallback<T> callback;
	protected SpawnState spawnState;
	protected SpawnMode spawnMode;
	protected ValueMode timeMode;
	protected ValueMode locationModeX;
	protected ValueMode locationModeY;
	protected ValueMode locationModeZ;
	long startTime;
	long timeDiff;
	long timeDiffMin;
	long timeDiffMax;
	float x, xMin, xMax;
	float y, yMin, yMax;
	float z, zMin, zMax;
	float rotationInRadians = 0f;
	protected int maxArraySize;
	
	/** this is the standard constructor for a single gameObject and a destination ArrayList */
	public SpawnTask(int ID, T gameObject, ArrayList<T> destArray) {
		initialize(ID, gameObject, null, destArray);
	}
		
	/** this is the standard constructor with a pool and a destination ArrayList */
	public SpawnTask(int ID, Pool<T> pool, ArrayList<T> destArray) {
		initialize(ID, null, pool, destArray);
	}
	
	/** this constructor is for pseudo SpawnObjects that should only trigger a callback */
	public SpawnTask(int ID, IspawnCallback<T> cb) {
		initialize(ID, null, null, null);
		setCallback(cb);
		locationModeX = ValueMode.EMPTY;
		locationModeY = ValueMode.EMPTY;
		locationModeZ = ValueMode.EMPTY;
	}
	
	public void initialize(int ID, T gameObject, Pool<T> pool, ArrayList<T> destArray) {
		this.sourcePool = pool;
		this.singleObjectToSpawn = gameObject;
		this.destinationArray = destArray;
		this.ID 	= ID;
		callback 	= null;
		startTime 	= System.nanoTime(); // muss bei PAUSE immer auf die startzeit addiert werden
		spawnState 	= SpawnState.NEW;
		spawnMode 	= SpawnMode.UNDEFINED;
		timeMode 	= ValueMode.UNDEFINED;
		locationModeX = ValueMode.UNDEFINED;
		locationModeY = ValueMode.UNDEFINED;
		setFixLocationModeZ(0f);	//--- default: 2D
		maxArraySize = ARRAY_SIZE_UNLIMITED;
	}	

	public void update(float deltatime) {

		if ((spawnMgr.now - startTime) > timeDiff) {
			
			T gameObject = getObject();
			if (gameObject != null) spawnObject(gameObject);
			callback(gameObject);	// callback might be called even though gameObject is NULL (only callback)
			setNext();
		}
	}
	
	protected T getObject() {
		T gameObject = null;
		
		if (sourcePool != null) {
			gameObject = sourcePool.obtain();
		}
		else if (singleObjectToSpawn != null) {
			gameObject = singleObjectToSpawn;
		}
		return gameObject;
	}
	
	// inform listeners via callback
	// callback might be called even though gameObject is NULL (only callback)
	protected boolean callback(T gameObject) {
		if (callback != null) {
			callback.reportSpawnedObject(gameObject);
			return true;
		}
		return false;
	}
	
	// sets the next continous element and returns true if continous
	protected boolean setNext() {
		if (spawnMode == SpawnMode.CONTINOUS) {
			startTime = spawnMgr.now;
			if (timeMode == ValueMode.RANDOM) 		timeDiff = getNewRandomTimeDiff();
			if (locationModeX == ValueMode.RANDOM) 	x = getNewRandomLocation(xMin, xMax);
			if (locationModeY == ValueMode.RANDOM) 	y = getNewRandomLocation(yMin, yMax);
			if (locationModeZ == ValueMode.RANDOM) 	z = getNewRandomLocation(zMin, zMax);
			
			//---- check if max size is achieved and pause spawntask
			if (maxArraySize > ARRAY_SIZE_UNLIMITED && destinationArray.size() >= maxArraySize) {
				pause();
//				Gdx.app.log("SPAWNTASK", "paused");
			}
			return true;
		} 
		else if (spawnMode == SpawnMode.ONCE) finish();
		return false;
	}
	
	protected void spawnObject(T gameObject) {
		gameObject.setSpawningPosition(x, y, z, rotationInRadians);
		gameObject.setActive();
		gameObject.doAfterSpawning();
		if (destinationArray != null) destinationArray.add(gameObject);	// might be null !! // this is for pseudo SpawnObjects that should only trigger a callback
//		Gdx.app.log("SpawnTask", "just spawned an object at: " + x + ", " + y + ", " + z);
	}
	
	/** Checks if the SpawnTask is correctly setup. 
	 * Used bei SpawnManager before task will be added and set as RUNNING */
	public boolean verify() {
		if (spawnMode == SpawnMode.UNDEFINED) 		return false;
		if (timeMode == ValueMode.UNDEFINED) 		return false;
		if (locationModeX == ValueMode.UNDEFINED) 	return false;
		if (locationModeY == ValueMode.UNDEFINED) 	return false;
		if (locationModeZ == ValueMode.UNDEFINED) 	return false;
		return true;
	}

	public SpawnTask<? extends Ispawnable> setCallback(IspawnCallback<T> cb) {
		this.callback = cb;
		return this;
	}
	
	public void addDelay(long pauseTime) {
		addPauseTime(pauseTime);
	}
	
	//---- used for gamestate ready / paused
	public void addPauseTime(long pauseTime) {
		startTime = startTime + pauseTime;
//		Gdx.app.log("addPauseTime", "" + pauseTime);
	}
	
	public void pause() {
		if (spawnState == SpawnState.RUNNING) spawnState = SpawnState.PAUSED;
	}
	public void restart() {
		if (spawnState == SpawnState.PAUSED) {
			spawnState = SpawnState.RUNNING;
			startTime = System.nanoTime();
		}
	}
	public void finish() {
		spawnState = SpawnState.FINISHED;
	}

	//================== getters ===============================
	
	/** Returns the ID. */
	public int getID() {
		return ID;
	}
	
	/** Returns the StartTime. */
	public long getStartTime() {
		return startTime;
	}
	
	/** Returns the SpawnState. */
	public SpawnState getSpawnState() {
		return spawnState;
	}

	/** Returns the SpawnMode. */
	public SpawnMode getSpawnMode () {
		return spawnMode;
	}

	protected float getNewRandomLocation(float min, float max) {
		return MathUtils.random(min, max);
	}
	
	protected long getNewRandomTimeDiff() {
		return MathUtils.random(timeDiffMin, timeDiffMax);
	}
	
	public int getDestinationArraySize() {
		return destinationArray.size();
	}
	
	//================== setters ===============================
	
	/** Sets the SpawnState.
	 * 
	 * @param spawnState  */
	protected void setSpawnState(SpawnState state) {
		this.spawnState = state;
	}
	
	/** Sets the SpawnMode.
	 * 
	 * @param spawnMode  */
	public SpawnTask<? extends Ispawnable> setSpawnMode(SpawnMode spawnMode) {
		setSpawnMode (spawnMode, -1);	// -1 is default for maxArraySize -> means unlimited
		return this;
	}

	/** Sets the SpawnMode and maxArraySize
	 * 
	 * @param spawnMode
	 * @param maxArraySize spawnTask will pause if array has maxArraySize. if array  goes below maxArraySize spawntask will restart.  */
	public SpawnTask<? extends Ispawnable> setSpawnMode (SpawnMode spawnMode, int maxArraySize) {
		this.spawnMode = spawnMode;
		if (spawnMode == SpawnMode.CONTINOUS) this.maxArraySize = maxArraySize; 
		else this.maxArraySize = ARRAY_SIZE_UNLIMITED;
		return this;
	}
	
	public SpawnTask<? extends Ispawnable> setFixLocationModeX(float x) {
		this.locationModeX = ValueMode.FIX;
		this.x = x;
		return this;
	}
	public SpawnTask<? extends Ispawnable> setFixLocationModeY(float y) {
		this.locationModeY = ValueMode.FIX;
		this.y = y;
		return this;
	}
	public SpawnTask<? extends Ispawnable> setFixLocationModeZ(float z) {
		this.locationModeZ = ValueMode.FIX;
		this.z = z;
		return this;
	}
	public SpawnTask<? extends Ispawnable> setRandomLocationModeX(float xMin, float xMax) {
		this.locationModeX = ValueMode.RANDOM;
		this.xMin = xMin;
		this.xMax = xMax;
		this.x = getNewRandomLocation(xMin, xMax);
		return this;
	}
	public SpawnTask<? extends Ispawnable> setRandomLocationModeY(float yMin, float yMax) {
		this.locationModeY = ValueMode.RANDOM;
		this.yMin = yMin;
		this.yMax = yMax;
		this.y = getNewRandomLocation(yMin, yMax);
		return this;
	}
	public SpawnTask<? extends Ispawnable> setRandomLocationModeZ(float zMin, float zMax) {
		this.locationModeZ = ValueMode.RANDOM;
		this.zMin = zMin;
		this.zMax = zMax;
		this.z = getNewRandomLocation(zMin, zMax);
		return this;
	}

	public SpawnTask<? extends Ispawnable> setFixTimeMode(long timeDiff) {
		this.timeMode = ValueMode.FIX;
		this.timeDiff = timeDiff;
		this.timeDiffMin = 0L;
		this.timeDiffMax = 0L;
		return this;
	}

	public SpawnTask<? extends Ispawnable> setRandomTimeMode(long timeDiffMin, long timeDiffMax) {
		this.timeMode = ValueMode.RANDOM;
		this.timeDiffMin = timeDiffMin;
		this.timeDiffMax = timeDiffMax;
		timeDiff = getNewRandomTimeDiff();
		return this;
	}

	public SpawnTask<? extends Ispawnable> setRotationInRadians(float rotationInRadians) {
		this.rotationInRadians = rotationInRadians;
		return this;
	}
		
	protected void setSpawnManager(MgrSpawnManager mgr) {
		this.spawnMgr = mgr;
	}

	@Override
	public void reset () {
		spawnState 		= SpawnState.NEW;
		spawnMode 		= SpawnMode.UNDEFINED;
		timeMode 		= ValueMode.UNDEFINED;
		locationModeX = ValueMode.UNDEFINED;
		locationModeY = ValueMode.UNDEFINED;
		locationModeZ = ValueMode.UNDEFINED;
		rotationInRadians = 0f;
		maxArraySize = ARRAY_SIZE_UNLIMITED;
	}
}
