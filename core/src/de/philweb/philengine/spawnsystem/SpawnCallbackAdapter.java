package de.philweb.philengine.spawnsystem;

/** Empty SpawnTask with SpawnCallback for the spawnsystem.
* this is for pseudo SpawnObjects that should only trigger a callback
* sets itself as callback and should override reportSpawnedObject(Ispawnable gameObject)
* 
* @author just4phil - heavily.loaded.games@googlemail.com */

public class SpawnCallbackAdapter extends SpawnTask<Ispawnable> implements IspawnCallback<Ispawnable> {

	public SpawnCallbackAdapter(int ID) {
		super(ID, null);
		setCallback(this);	// sets itself as callback
	}

	@Override
	public void reportSpawnedObject(Ispawnable gameObject) { }
}
