package de.philweb.philengine.spawnsystem;

/** Interface for the spawnsystem.
 * objects that shall be spawned must implement this Interface
 * 
 * @author just4phil - heavily.loaded.games@googlemail.com */

public interface Ispawnable {
	 
	public void setObjectID(int id);
	public void setSpawningPosition(float x, float y, float z, float rotationInRadians);
	public void setActive();
	public void doAfterSpawning();
}
