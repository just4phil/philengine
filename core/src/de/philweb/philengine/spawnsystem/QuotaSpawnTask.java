package de.philweb.philengine.spawnsystem;

/** SpawnTask for the spawnsystem.
* manages all a concrete spawntask
* 
* @author just4phil - heavily.loaded.games@googlemail.com */

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Pool;

public class QuotaSpawnTask<T extends Ispawnable> extends SpawnTask<T> {
		
	int sumOfPriorities = 0;
	float sumOfQuotas = 0f; 	// FIXME: not needed!  just for debugging
	float quota = 0f; 		// FIXME: not needed!  just for debugging
	ArrayList<QuotaElement> quotaElements = new ArrayList<QuotaElement>();
    Random random;	
	float rand = 0f;
	int setObjectID = -1;
	float startFloat;
	float endFloat;
	
	/** this is the standard constructor with a pool and a destination ArrayList */
	public QuotaSpawnTask(int ID, Pool<T> pool, ArrayList<T> destArray) {
		super(ID, pool, destArray);
		random = new Random();
	}	
	
	@Override
	public void update(float deltatime) {

		if ((spawnMgr.now - startTime) > timeDiff) {
			
			T gameObject = getObject();
			if (gameObject != null) {
				
				//--- get a random type correspondig to the quotas of elements in list
				rand = random.nextFloat();
				setObjectID = -1;
				startFloat = 0f;
				for (QuotaElement element : quotaElements) {
					if (element.isActive()) {
						endFloat = startFloat + element.getQuota();
						if (startFloat <= rand && rand < endFloat) {
							setObjectID = element.getID();
							break;
						}
						else startFloat = endFloat;			
					}
				}
				if (setObjectID != -1) {
					gameObject.setObjectID(setObjectID);	// set the randomly found new type!
					spawnObject(gameObject);
				}
				else Gdx.app.log("Error", "QuotaSpawnTask: setObjectID == -1");
			}
			
			callback(gameObject);	// callback might be called even though gameObject is NULL (only callback)
			setNext();
		}
	}

	//--- these elements are active by default
	public boolean addQuotaElement(int ID, int priority) {	// eine ObjectID darf nur einmal vorkommen!
		return addQuotaElement(new QuotaElement(ID, priority));
	}
	
	//--- here you can define if the element is active or not
	public boolean addQuotaElement(int ID, int priority, boolean isActive) {	// eine ObjectID darf nur einmal vorkommen!
		return addQuotaElement(new QuotaElement(ID, priority, isActive));
	}
	
	public boolean addQuotaElement(QuotaElement element) {	// eine ObjectID darf nur einmal vorkommen!
		for (QuotaElement el : quotaElements)
			if (el.getID() == element.getID()) {
				Gdx.app.log("Error", "addQuotaElement: ID already existing! Did not add the element.");
				return false;
			}
		quotaElements.add(element);
		element.setQuotaSpawnTask(this);
		updateQuotas();
		return true;
	}

	// calculates the sum of priorities and afterwards the quota for each element
	public void updateQuotas() {
		sumOfPriorities = 0;
		sumOfQuotas = 0f;
		for (QuotaElement el : quotaElements) {
			if (el.isActive()) sumOfPriorities = sumOfPriorities + el.getPriority();
		}
		for (QuotaElement el : quotaElements) {
			if (el.isActive()) {
				quota = (float)el.getPriority() / (float)sumOfPriorities;
				sumOfQuotas = sumOfQuotas + quota;
				el.setQuota(quota);	
			}
		}
//		Gdx.app.log("updateQuotas()", "sumOfPriorities: " + sumOfPriorities + " // sumOfQuotas: " + sumOfQuotas);	// FIXME: just for debugging
	}

	public QuotaElement getQuotaElement(int ID) {	// eine ObjectID darf nur einmal vorkommen!
		QuotaElement returnElement = null;
		for (QuotaElement el : quotaElements)
			if (el.getID() == ID) {
				returnElement = el;
				break;
			}
		return returnElement;
	}
	
	public boolean pauseQuotaElement(int ID) {
		return pauseQuotaElement(getQuotaElement(ID));
	}
	public boolean pauseQuotaElement(QuotaElement element) {	// eine ObjectID darf nur einmal vorkommen!
		if (element != null) {
			if (quotaElements.contains(element)) {
				element.setActive(false);
				updateQuotas();
				return true;
			}
		}
		return false;
	}
	
	public boolean restartQuotaElement(int ID) {
		return restartQuotaElement(getQuotaElement(ID));
	}
	public boolean restartQuotaElement(QuotaElement element) {	// eine ObjectID darf nur einmal vorkommen!
		if (element != null) {
			if (quotaElements.contains(element)) {
				element.setActive(true);
				updateQuotas();
				return true;
			}
		}
		return false;
	}
	
	public boolean removeQuotaElement(int ID) {
		return removeQuotaElement(getQuotaElement(ID));
	}
	public boolean removeQuotaElement(QuotaElement element) {	// eine ObjectID darf nur einmal vorkommen!
		if (element != null) {
			if (quotaElements.contains(element)) {
				quotaElements.remove(element);	
				element.setQuotaSpawnTask(null);
				updateQuotas();
				return true;
			}
		}
		return false;
	}
	
//	/** Checks if the SpawnTask is correctly setup. 
//	 * Used bei SpawnManager before task will be added and set as RUNNING */
//	@Override
//	public boolean verify() {
//		if (spawnMode == SpawnMode.UNDEFINED) 		return false;
//		if (timeMode == ValueMode.UNDEFINED) 		return false;
//		if (locationModeX == ValueMode.UNDEFINED) 	return false;
//		if (locationModeY == ValueMode.UNDEFINED) 	return false;
//		if (locationModeZ == ValueMode.UNDEFINED) 	return false;
//		return true;
//	}

//	@Override
//	public void reset () {
//		spawnState 		= SpawnState.NEW;
//		spawnMode 		= SpawnMode.UNDEFINED;
//		timeMode 		= ValueMode.UNDEFINED;
//		locationModeX = ValueMode.UNDEFINED;
//		locationModeY = ValueMode.UNDEFINED;
//		locationModeZ = ValueMode.UNDEFINED;
//		rotationInRadians = 0f;
//		maxArraySize = ARRAY_SIZE_UNLIMITED;
//	}
}
