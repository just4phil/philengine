package de.philweb.philengine.particles2d;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;


public class ParticleFXPool extends PEParticleEffectPool {

	private int ID;
	private int initialCapacity;
	private boolean isAdditive = false;
	
	public ParticleFXPool(int ID, ParticleEffect effect, int initialCapacity, int max, boolean isAdditive, boolean fillPool) {    	
		super(effect, initialCapacity, max);
		this.initialCapacity = initialCapacity;
		setID(ID);
		setAdditive(isAdditive);
		if (fillPool) fillPool();	// fill pool and remove elements again just to have a full pool
	}
	
	private void setID(int ID) {
		this.ID = ID;
	}
	public int getID() {
		return ID;
	}

	public boolean isAdditive() {
		return isAdditive;
	}
	public void setAdditive(boolean isAdditive) {
		this.isAdditive = isAdditive;
	}
	
	// fill pool and remove elements again just to have a full pool
	void fillPool() {
		ArrayList<PooledEffect> list = new ArrayList<PooledEffect>();
		int i;
		//----- generate ----------------------
		for (i = 0; i < initialCapacity; i++) {
			PooledEffect fx = obtain();
			list.add(fx);
		}
		//----- free pool ----------------------
		for (i = 0; i < list.size(); i++) {
			PooledEffect fx = list.get(i);
			free(fx);	
		}
		list.clear();
	}
}
