package de.philweb.philengine.particles2d;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import de.philweb.philengine.particles2d.PEParticleEffectPool.PooledEffect;

//========================================================================
//
// TODO: premultipliedAlpha -> particleEmitter.setPremultipliedAlpha(...)
// 
// DONT MIX ADDITIVE-PartFX and NONADDITIVE-PartFX IN ONE PartEMITTER!!
//


public class MgrParticleFxManager {
	
	private boolean debug;
	private boolean isParticlesEnabled;
	private int arraySize, i;
	private ArrayList<ParticleFXPool> poolList;
	private ArrayList<PooledEffect> additiveBlendingfxList;		// concrete fx to update and render
	private ArrayList<PooledEffect> nonAdditiveBlendingfxList;	// concrete fx to update and render
	private PooledEffect particleEffect;

	public MgrParticleFxManager() {
		initialize(false);
	}
	public MgrParticleFxManager(boolean isDebug) {
		setDebug(isDebug);
		if (isDebug) Gdx.app.log("ParticleFxManager", "new ParticleFxManager() constructed");
		initialize(isDebug);
	}
    
	private void initialize(boolean isDebug) {
		setDebug(isDebug);
		isParticlesEnabled 			= true;		// must be set at first (if set in initialize it overrides later changes)
		poolList					= new ArrayList<ParticleFXPool>();
		additiveBlendingfxList 		= new ArrayList<PooledEffect>();
		nonAdditiveBlendingfxList 	= new ArrayList<PooledEffect>();
	}
	
	//============================== FIXME: scale is here workaround weil setScale in startNewFX (noch) buggy ist !!!!
	/** used to define your particleFX types. For each type a pool will be constructed.
	 * @param ID is a int ID that must be a unique identifier
	 * @param file is a path string to the particle file
	 * @param atlas is a textureatlas that contains the textureregion of the particle image that is specified in the particle file
	 * @param initialCapacity is the min-capacity of the pool
	 * @param max is the max size of the pool
	 * @param isAdditive defines if the particles should be rendered as additive color
	 * @param fillPool if true the pool will be initialized with objects. that may take some time.*/
    public void addFXpool(int ID, String file, TextureAtlas atlas, int initialCapacity, int max, boolean isAdditive, boolean fillPool, float scale) {
    	if (isParticlesEnabled) {
	    	ParticleEffect particleEffect = new ParticleEffect();	
	    	particleEffect.load(Gdx.files.internal(file), atlas);
	    	
	    	particleEffect.scaleEffect(scale);	//=== FIXME: scale is here workaround weil setScale in startNewFX (noch) buggy ist !!!!
	    	
	    	//--- performance-optim.: disable automatic blend function of .draw!
	    	//  nicht mehr noetig seit libgdx 1.5.5. // https://github.com/libgdx/libgdx/pull/2888
//	    	for (ParticleEmitter particleEmitter : particleEffect.getEmitters()) {
//	    		particleEmitter.setAdditive(false);	//--- performance-optim.: disable automatic blend function of .draw!
//	    	}	    	
	    	particleEffect.setEmittersCleanUpBlendFunction(false); //  neu seit libgdx 1.5.5. // https://github.com/libgdx/libgdx/pull/2888
	    	
	    	poolList.add(new ParticleFXPool(ID, particleEffect, 20, 50, isAdditive, fillPool));
	    	if (debug) Gdx.app.log("ParticleFxManager", "added new FXpool // poolList.size: " + poolList.size());
    	}
    }
    
    // aus liste aller typen pool finden und fx objekt ziehen
    private ParticleFXPool getFXpool(int ID) {    	
		arraySize = poolList.size();
		for (i = 0; i < arraySize; i++) {
			ParticleFXPool fxPool = poolList.get(i);
			if (fxPool.getID() == ID) return fxPool;
		}
		return null;
    }
    
    private PooledEffect getNewFX(int ID) {
    	ParticleFXPool pool = getFXpool(ID);
    	if (pool != null) {
//    		if (debug) Gdx.app.log("pool ID " + ID, "pool.size: " + pool.getFree() + " // pool.peak: " + pool.peak);
    		return pool.obtain();
    	}
    	else {
    		if (debug) Gdx.app.log("ParticleFxManager", "ERROR: getNewFX: pool == null, ID:" + ID);
    		return null;
    	}
    }
        
  	/** used to start a concrete FX. a pooled fx will be taken and positioned. This FX will follow the objectToFollow.
  	 * @param ID is a int ID that must be a unique identifier
  	 * @param objectToFollow if set the particle effects follows this object if it is moving
  	 * @see #IparticlePosition
  	 * @param scale scales the effect */
      public PooledEffect startNewFX(int ID, float x, float y, float scale) {
    	  return startNewFX(ID, null, x, y, scale);
      }
      
  	/** used to start a concrete FX. a pooled fx will be taken and positioned. This FX will follow the objectToFollow.
  	 * @param ID is a int ID that must be a unique identifier
  	 * @param objectToFollow if set the particle effects follows this object if it is moving
  	 * @see #IparticlePosition
  	 * @param scale scales the effect */
      public PooledEffect startNewFX(int ID, IparticlePosition objectToFollow, float scale) {
    	  return startNewFX(ID, objectToFollow, objectToFollow.getPosition().x, objectToFollow.getPosition().y, scale);
      }
         
	/** used to start a concrete FX. a pooled fx will be taken and positioned.
	 * @param ID is a int ID that must be a unique identifier
	 * @param x, y is the position
	 * @param scale scales the effect
	 * @param allowCompletion set to true if a continous effect should stop. Ignores the {@link #setContinuous(boolean) continuous} setting until the emitter is started again. This allows the emitter to stop smoothly. */
    private PooledEffect startNewFX(int ID, IparticlePosition objectToFollow, float x, float y, float scale) {
    	if (isParticlesEnabled) {
	    	PooledEffect fx = getNewFX(ID);
			// pool.obtain() already calls reset() -> reset calls start() -> start calls restart()
	  		// start() sets allowCompletion = false!
	    	if (fx != null) {
//	    		if (debug) Gdx.app.log("pool ID " + ID, "pool.size: " + getFXpool(ID).getFree() + " // pool.peak: " + getFXpool(ID).peak);
  	
//	    		fx.scaleEffect(scale);	// !!!! BUG!! FIXME: scale fuehrt dazu dass objekte nicht korrekte resettet werden!!!!!
		    	fx.setPosition(x, y);
		    	fx.objectToFollow = objectToFollow;
		    	
		    	if (getFXpool(ID).isAdditive()) additiveBlendingfxList.add(fx);
		    	else nonAdditiveBlendingfxList.add(fx);
	    	}
	    	else if (debug) Gdx.app.log("ParticleFxManager", "ERROR: startNewFX: fx == null, ID:" + ID);
	    	return fx;
    	}
    	return null;
    }
       
 // macht nur sinn weahrend effekt laueft
    public void allowCompletion(PooledEffect fx) {
   	 	if (isParticlesEnabled && fx != null) fx.allowCompletion();
    }
    
    public void setContinuous(PooledEffect fx, boolean continous) {
      	 if (isParticlesEnabled && fx != null) {
      		 for (int i = 0, n = fx.getEmitters().size; i < n; i++) fx.getEmitters().get(i).setContinuous(continous);
      	 }
    }
    
	/** clears all the arrayLists of the manager
	 * <br>
	 * use this right before you set up your FXpools at start/restart of the game or level */
    public void clear() {
    	initialize(debug);
    }
    
    public MgrParticleFxManager setDebug(boolean isDebug) {
    	this.debug = isDebug;
    	return this;
    }
    
    public boolean isParticlesEnabled() {
    	return isParticlesEnabled;
    }
    
    public void setParticlesEnabled(boolean isParticlesEnabled) {
    	this.isParticlesEnabled = isParticlesEnabled;
    }
     
    public void update(float deltaTime) {
    	if (isParticlesEnabled) {
    		arraySize = additiveBlendingfxList.size();			
    		for (i = 0; i < arraySize; i++) {
				particleEffect = additiveBlendingfxList.get(i);
		
				if (particleEffect.isComplete()) {
					particleEffect.free();	// .free() does not call .reset() because object is not instance of poolable		
					additiveBlendingfxList.remove(particleEffect);
					arraySize = additiveBlendingfxList.size();
				}
				else particleEffect.update(deltaTime);	// sets Position to ObjectToFollow, too (if not null)
			}
			arraySize = nonAdditiveBlendingfxList.size();			
			for (i = 0; i < arraySize; i++) {
				particleEffect = nonAdditiveBlendingfxList.get(i);
		
				if (particleEffect.isComplete()) {
					particleEffect.free();	// .free() does not call .reset() because object is not instance of poolable
					nonAdditiveBlendingfxList.remove(particleEffect);
					arraySize = nonAdditiveBlendingfxList.size();
				}
				else particleEffect.update(deltaTime);	// sets Position to ObjectToFollow, too (if not null)
			}
    	}
    }
        
    public void render(SpriteBatch batch) {
    	if (isParticlesEnabled) {
	    	arraySize = additiveBlendingfxList.size();
//			if (arraySize > 0) {
//				batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);	// performance-optim.: manually set blend function to additive!... nicht mehr noetig seit libgdx 1.5.5.  // https://github.com/libgdx/libgdx/pull/2888
				
					for (i = 0; i < arraySize; i++) additiveBlendingfxList.get(i).draw(batch);
				
				batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);	// performance-optim.: manually reset blend function!  // https://github.com/libgdx/libgdx/pull/2888
//			}
//			if (debug) Gdx.app.log("", "arraySize: " + arraySize + " // draw-calls: " + batch.renderCalls);
				
			arraySize = nonAdditiveBlendingfxList.size();
			for (i = 0; i < arraySize; i++) nonAdditiveBlendingfxList.get(i).draw(batch);
    	}
    }   
    	
    // call this after usage to free ressources
    public void dispose() {
   	 if (debug) Gdx.app.log("ParticleFxManager", "disposed.");
    }
}
