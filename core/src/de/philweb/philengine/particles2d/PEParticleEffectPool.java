/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package de.philweb.philengine.particles2d;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

import de.philweb.philengine.particles2d.PEParticleEffectPool.PooledEffect;

public class PEParticleEffectPool extends Pool<PooledEffect> {
	private final ParticleEffect effect;

	public PEParticleEffectPool (ParticleEffect effect, int initialCapacity, int max) {
		super(initialCapacity, max);
		this.effect = effect;
	}

	protected PooledEffect newObject () {
		return new PooledEffect(effect);
	}

	public PooledEffect obtain () {
		PooledEffect effect = super.obtain();
		effect.reset();
		return effect;
	}

	public class PooledEffect extends ParticleEffect {
		
		protected IparticlePosition objectToFollow = null;
		protected Array<ParticleEmitter> emitters;
		
		PooledEffect (ParticleEffect effect) {
			super(effect);
			emitters = getEmitters();
		}

		@Override
		public void update (float delta) {
			for (int i = 0, n = emitters.size; i < n; i++) {
				ParticleEmitter emitter = emitters.get(i);
				if (objectToFollow != null) emitter.setPosition(objectToFollow.getPosition().x, objectToFollow.getPosition().y);
				emitter.update(delta);
			}
		}
				
		@Override
		public void reset () {
			super.reset();
			objectToFollow = null;
			emitters = getEmitters();
		}

		public void free () {
			PEParticleEffectPool.this.free(this);
		}
	}
}
