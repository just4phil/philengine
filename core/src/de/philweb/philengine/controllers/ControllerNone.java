package de.philweb.philengine.controllers;

import de.philweb.philengine.common.Game;

public class ControllerNone extends AbstractController {

	public ControllerNone(Game game, String name) {
		super(game, name);										
	}
	
	@Override
	public String getDescription() {
		return MgrControllerConfigManager.ControllerNONE;
	}
}
