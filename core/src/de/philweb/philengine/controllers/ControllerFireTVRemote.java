package de.philweb.philengine.controllers;

import com.badlogic.gdx.Gdx;

import de.philweb.philengine.common.Game;


public class ControllerFireTVRemote extends AbstractController {
	
	public ControllerFireTVRemote(Game game, String name) {
		super(game, name);										
	}
	
	@Override
	public String getDescription() {
		return name;
	}	
	
	@Override 
	public float getInputForCommand(int cmd) {
		
		returnfloat = RETURNFALSE;
		Command command = controllerConfig.arrayCommands[cmd];	// get reference to command-object in array
		
		if (command.ID != cmd) {
			Gdx.app.log("getInput: error!", "command.ID != cmd");
			return returnfloat;
		}

		if (command.buttonIndex != MgrControllerConfigManager.UNCONFIGURED) {
			if (command.type == Command.TYPE_KEY) {
				switch(command.character) {
					case Command.CHARACTER_UP:
						returnfloat = getControllerButtonUp(command.buttonIndex);
						break;
					case Command.CHARACTER_DOWN:
						returnfloat = getControllerButtonDown(command.buttonIndex);
						break;
				}	
			}
		}
		return returnfloat;
	}
	
	// wrapper to convert booleans to floats
	public float getControllerButtonDown(int key) {	
		if (Gdx.input.isKeyPressed(key)) return 1f;
		else return 0f;
	}
	
	// wrapper to convert booleans to floats
	public float getControllerButtonUp(int key) {	
		if (Gdx.input.isKeyJustPressed(key)) return 1f;
		else return 0f;
	}
	
//	@Override
//	public String getButtonToIndex(int index) {
//		return KeyEvent.getKeyText(index);
//	}
}
