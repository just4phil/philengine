package de.philweb.philengine.controllers;

import com.badlogic.gdx.Gdx;

import de.philweb.philengine.common.Game;


public class ControllerAndroid extends AbstractController {
	
	float calculatedVELOCITY;
	float accel;
	
//	final static float calibrationMax = 3.2858f;	// WIRD NICHT BENÖTIGT
//	final static float calibrationAndroidMax = 3.2858f;				// ab faktor 8 ist volle velocity, damit man den joxstick nicht voll ausschlagen muss
//	final static float calibrationFactor = 1.75f;									// gibt quasi an, wie weit der joystick / das handy bewegt werden muss f�r volle velocity
//	final static float correctionFactor = 1.0f;	// 1.0f;
//	final static float normalization = calibrationAndroidMax / calibrationMax;	// auf gleiche range wie bei android normalisieren!
	
	final static float deadZone = 0.05f; 				// grenze +/- zum drehen der blickrichtung
	final static float borderWalk = 0.1f; 				// grenze +/- zum starten des laufens
	final static float accel_calib_min = 0f;
	final static float accel_calib_max = 10f;
	//---------------------------------------------------------
	
	public ControllerAndroid(Game game, String name) {
		super(game, name);    
	}
	
	public String getDescription() {
		return MgrControllerConfigManager.ControllerAndroid;
	}

	@Override 
	public float getInputForCommand(int cmd) {
		
		returnfloat = RETURNFALSE;
		Command command = controllerConfig.arrayCommands[cmd];	// get reference to command-object in array
		
		if (command.ID != cmd) {
			Gdx.app.log("getInput: error!", "command.ID != cmd");
			return returnfloat;
		}
		
		// let abstractController check the touchzone because on ouya and desktop touch works also
//		if (command.buttonIndex != MgrControllerConfigManager.UNCONFIGURED) {
		if (command.type == Command.TYPE_TOUCH) returnfloat = getTouchForCommand(command);
		
		else if (command.type == Command.TYPE_AXIS) returnfloat = getAccelerometerForCommand(command);
		
		return returnfloat;
	}
	
	public float getAccelerometerForCommand (Command command) {
		returnfloat = RETURNFALSE;

		switch (command.character) {
		case Command.CHARACTER_AXIS_X:
			accel = Gdx.input.getAccelerometerX();
			break;
		case Command.CHARACTER_AXIS_Y:
			accel = Gdx.input.getAccelerometerY();
			break;
		case Command.CHARACTER_AXIS_Z:
			accel = Gdx.input.getAccelerometerZ();
			break;
		}
		
//		accel = (Gdx.input.getAccelerometerY() - accel_calib_min ) / (accel_calib_max  - accel_calib_min );
		accel = accel / accel_calib_max;		// get value and calibrate
		if (accel >= -deadZone && accel <= deadZone) {	// check for deadZone
			accel = 0.0f;
		}
		else {
			switch (command.returnType) {
			
			case Command.RETURN_POSITIVE:
				if (accel > 0 && Math.abs(accel) > command.treshold) returnfloat = Math.abs(accel);
				break;
	
			case Command.RETURN_NEGATIVE:
				if (accel < 0f && Math.abs(accel) > command.treshold) returnfloat = Math.abs(accel);
				break;
			}
		}
		return returnfloat;
	}
}
