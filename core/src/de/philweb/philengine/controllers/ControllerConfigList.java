package de.philweb.philengine.controllers;

import java.util.ArrayList;


public class ControllerConfigList {

	public ArrayList<ControllerConfig2> list = new ArrayList<ControllerConfig2>();
	

	
	
	public String[] getStringList() {
		
		int size = list.size();
		String[] stringList = new String[size];
		
		for (int i = 0; i < size; i++) {
			
			stringList[i] = list.get(i).name;
		}
		
		return stringList;
		
	}
	
	

	public int size() {
		
		return list.size();
	}

	
	public void addConfig(ControllerConfig2 controllerConfig) {
		
		list.add(controllerConfig);

	}
	
	public void deleteConfig(ControllerConfig2 controllerConfig) {
		
		list.remove(controllerConfig);

	}
	
}
