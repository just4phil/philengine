package de.philweb.philengine.controllers;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;

import de.philweb.philengine.common.Game;


public class ControllerGamepad extends AbstractController {

	ArrayList<Integer> arrayButtonHasBeenPressed = new ArrayList<Integer>();
	ArrayList<Integer> arrayAxisHasBeenPressed = new ArrayList<Integer>();
	float controllerAxisUp;
	float returnFloat;
//	boolean lastAxisWasNegative = false;
	
	/**
	 * use setController(controller) after this constructor!
	 * @author andre
	 */
	public ControllerGamepad(Game game, String name) {
		super(game, name);
	}
	
	/**
	 * This Constructor sets the controller automatically
	 * @author andre
	 */
	public ControllerGamepad(Game game, Controller controller) {
		super(game, controller.getName());
		setController(controller);
	}
	
	@Override
	public String getDescription() {
		return controller.getName();
	}
	
	@Override
	public String getButtonToIndex(int index) {			// TODO // FIXME ---------------- non functional!!!!
		button = "";
//		if (MgrControllerConfigManager.ControllerSaitek.equals(name)) 	button = "";
		if (OuyaCodes.ID.equals(name))	button = OuyaCodes.getButtonToIndex(index);	
		return button;
	}
	
	@Override 
	public float getInputForCommand(int cmd) {
		
		returnfloat = RETURNFALSE;
		Command command = controllerConfig.arrayCommands[cmd];	// get reference to command-object in array 
							// hat hier mal gecrasht: 26.02.2015 :
							// 0	java.lang.NullPointerException
							// 1	at de.philweb.philengine.controllers.ControllerGamepad.getInputForCommand(ControllerGamepad.java:53)
							// 2	at de.philweb.game2.screens.GameScreen.update(GameScreen.java:528)
		
		if (command.ID != cmd) {
			Gdx.app.log("getInput: error!", "command.ID != cmd");
			return returnfloat;
		}
			
		if (command.buttonIndex != MgrControllerConfigManager.UNCONFIGURED) {
			switch(command.type) {
	
				case Command.TYPE_BUTTON:
					
					switch(command.character) {
	
						case Command.CHARACTER_UP:
							returnfloat = super.getTouchUp(command.touchZone);	// let abstractController check the touchzone because on ouya and desktop touch works also	
							if (returnfloat == RETURNFALSE) returnfloat = getControllerButtonUp(command.buttonIndex);
							break;
							
						case Command.CHARACTER_DOWN:
							returnfloat = checkSingleTouchDownInRectangle(command.touchZone);
							returnfloat = super.getTouchForCommand(command);	// let abstractController check the touchzone because on ouya and desktop touch works also
							if (returnfloat == RETURNFALSE) returnfloat = getControllerButtonDown(command.buttonIndex);
							break;
					}			
					break;
	 
				case Command.TYPE_AXIS:
					
//					axisValue = controller.getAxis(command.buttonIndex);
					
					switch(command.character) {
					
					case Command.CHARACTER_UP:
						
						axisValue = getControllerAxisUp(command);
						
						switch(command.returnType) {
													
							case Command.RETURN_NEGATIVE:
								if (axisValue < 0) {
									returnfloat = 1f;
//									screen.showText("RETURN_NEGATIVE // axisValue < 0");
								}
								break;
								
							case Command.RETURN_POSITIVE:
								if (axisValue > 0) {
									returnfloat = 1f;
//									screen.showText("RETURN_POSITIVE // axisValue > 0");
								}
								break;
						}
						break;
						
					case Command.CHARACTER_DOWN:
						
						axisValue = controller.getAxis(command.buttonIndex);
						
						switch(command.returnType) {
						
							case Command.RETURN_NEGATIVE:
								if (axisValue < 0 && Math.abs(axisValue) > command.treshold) returnfloat = Math.abs(axisValue);
								break;
								
							case Command.RETURN_POSITIVE:
								if (axisValue > 0 && Math.abs(axisValue) > command.treshold) returnfloat = Math.abs(axisValue);
								break;
						}
						break;
				}	
			}
		}
//		screen.showText("returnfloat: " + returnfloat);
		return returnfloat;
	}

	// wrapper to convert booleans to floats
	public float getControllerButtonDown(int cmd) {	
		if (controller != null) {
			if (controller.getButton(cmd)) return 1f;
			else return 0f;
		}
		else return 0f;
	}
	
	// wrapper to convert booleans to floats
	public float getControllerButtonUp(int buttonIndex) {	
		float returnFloat = RETURNFALSE;
		
		if (controller != null) {
			if (controller.getButton(buttonIndex)) {
				if (!arrayButtonHasBeenPressed.contains((int)buttonIndex)) arrayButtonHasBeenPressed.add(buttonIndex);
				returnFloat = 0f;
			}
			else {
				if (arrayButtonHasBeenPressed.contains((int)buttonIndex)) { 
					arrayButtonHasBeenPressed.remove((Object)buttonIndex);
					returnFloat = 1f;
				}
			}
		}
		return returnFloat;
	}
	
		
	public float getControllerAxisUp(Command cmd) {	
		returnFloat = RETURNFALSE;
		
		if (controller != null) {
			
			int axisIndex = cmd.buttonIndex;
			controllerAxisUp = controller.getAxis(axisIndex);
			float controllerAxisUpPositive = Math.abs(controllerAxisUp);
			
			// code the direction into the Index
			if (cmd.returnType == Command.RETURN_NEGATIVE) axisIndex = axisIndex * -1;
						
			if (controllerAxisUpPositive > 0.15f) {
				
				returnFloat = 0f;
				
				if (cmd.returnType == Command.RETURN_NEGATIVE && controllerAxisUp > 0 || 
					cmd.returnType == Command.RETURN_POSITIVE && controllerAxisUp < 0 ) {
					return returnFloat;
				}

				if (!arrayAxisHasBeenPressed.contains((int)axisIndex)) {
					arrayAxisHasBeenPressed.add(axisIndex);
//					screen.showText("axis saved to array: " + axisIndex);
				}
			}
			else {
				if (arrayAxisHasBeenPressed.contains((int)axisIndex)) { 
					arrayAxisHasBeenPressed.remove((Object)axisIndex);
					
//					screen.showText("axis found in array: " + axisIndex);
					
					if (cmd.returnType == Command.RETURN_NEGATIVE) returnFloat = -1f;
					else returnFloat = 1f;
					
//					screen.showText("returnFloat: " + returnFloat);
				}
			}
		}
		return returnFloat;
	}
	
}
