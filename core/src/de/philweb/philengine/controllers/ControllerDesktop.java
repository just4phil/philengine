package de.philweb.philengine.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.IntMap;

import de.philweb.philengine.common.Game;


public class ControllerDesktop extends AbstractController {
	
	public ControllerDesktop(Game game, String name) {
		super(game, name);										
	}
	
	@Override
	public String getDescription() {
		return name;
	}	
	
	@Override 
	public float getInputForCommand(int cmd) {
		
		returnfloat = RETURNFALSE;
		Command command = controllerConfig.arrayCommands[cmd];	// get reference to command-object in array
		
		if (command.ID != cmd) {
			Gdx.app.log("getInput: error!", "command.ID != cmd");
			return returnfloat;
		}

		if (command.buttonIndex != MgrControllerConfigManager.UNCONFIGURED) {
			if (command.type == Command.TYPE_KEY) {
				switch(command.character) {
					case Command.CHARACTER_UP:
						// let abstractController check the touchzone because on ouya and desktop touch works also
						if (command.touchZone != null) returnfloat = super.getTouchUp(command.touchZone);
						// check keyboard
						if (returnfloat == RETURNFALSE) returnfloat = getControllerButtonUp(command.buttonIndex);
						break;
					case Command.CHARACTER_DOWN:
						// let abstractController check the touchzone because on ouya and desktop touch works also
						if (command.touchZone != null) returnfloat = super.checkMultiTouchInRectangle(command.touchZone, 1, command.lengthForHundredPercent);
						// check keyboard
						if (returnfloat == RETURNFALSE) returnfloat = getControllerButtonDown(command.buttonIndex);
						break;
				}	
			}
		}

		return returnfloat;
	}
	
	// wrapper to convert booleans to floats
	public float getControllerButtonDown(int key) {	
		if (Gdx.input.isKeyPressed(key)) return 1f;
		else return 0f;
	}
	
	// wrapper to convert booleans to floats
	public float getControllerButtonUp(int key) {	
		if (Gdx.input.isKeyJustPressed(key)) return 1f;
		else return 0f;
	}
	
	public static IntMap<String> mapKeysToNames = new IntMap<String>();
	static {
	  mapKeysToNames.put(-1, "unconfigured");
		mapKeysToNames.put(7, "NUM_0");
		mapKeysToNames.put(8, "NUM_1");
		mapKeysToNames.put(9, "NUM_2");
		mapKeysToNames.put(10, "NUM_3");
		mapKeysToNames.put(11, "NUM_4");
		mapKeysToNames.put(12, "NUM_5");
		mapKeysToNames.put(13, "NUM_6");
		mapKeysToNames.put(14, "NUM_7");
		mapKeysToNames.put(15, "NUM_8");
		mapKeysToNames.put(16, "NUM_9");
		mapKeysToNames.put(29, "A");
		mapKeysToNames.put(57, "ALT_LEFT");
		mapKeysToNames.put(58, "ALT_RIGHT");
		mapKeysToNames.put(75, "APOSTROPHE");
		mapKeysToNames.put(77, "AT");
		mapKeysToNames.put(30, "B");
		mapKeysToNames.put(4, "BACK");
		mapKeysToNames.put(73, "BACKSLASH");
		mapKeysToNames.put(31, "C");
		mapKeysToNames.put(5, "CALL");
		mapKeysToNames.put(27, "CAMERA");
		mapKeysToNames.put(28, "CLEAR");
		mapKeysToNames.put(55, "COMMA");
		mapKeysToNames.put(32, "D");
		mapKeysToNames.put(67, "DEL");
		mapKeysToNames.put(67, "BACKSPACE");
		mapKeysToNames.put(112, "FORWARD_DEL");
		mapKeysToNames.put(23, "DPAD_CENTER");
		mapKeysToNames.put(20, "DPAD_DOWN");
		mapKeysToNames.put(21, "DPAD_LEFT");
		mapKeysToNames.put(22, "DPAD_RIGHT");
		mapKeysToNames.put(19, "DPAD_UP");
		mapKeysToNames.put(23, "CENTER");
		mapKeysToNames.put(20, "DOWN");
		mapKeysToNames.put(21, "LEFT");
		mapKeysToNames.put(22, "RIGHT");
		mapKeysToNames.put(19, "UP");
		mapKeysToNames.put(33, "E");
		mapKeysToNames.put(6, "ENDCALL");
		mapKeysToNames.put(66, "ENTER");
		mapKeysToNames.put(65, "ENVELOPE");
		mapKeysToNames.put(70, "EQUALS");
		mapKeysToNames.put(64, "EXPLORER");
		mapKeysToNames.put(34, "F");
		mapKeysToNames.put(80, "FOCUS");
		mapKeysToNames.put(35, "G");
		mapKeysToNames.put(68, "GRAVE");
		mapKeysToNames.put(36, "H");
		mapKeysToNames.put(79, "HEADSETHOOK");
		mapKeysToNames.put(3, "HOME");
		mapKeysToNames.put(37, "I");
		mapKeysToNames.put(38, "J");
		mapKeysToNames.put(39, "K");
		mapKeysToNames.put(40, "L");
		mapKeysToNames.put(71, "LEFT_BRACKET");
		mapKeysToNames.put(41, "M");
		mapKeysToNames.put(90, "MEDIA_FAST_FORWARD");
		mapKeysToNames.put(87, "MEDIA_NEXT");
		mapKeysToNames.put(85, "MEDIA_PLAY_PAUSE");
		mapKeysToNames.put(88, "MEDIA_PREVIOUS");
		mapKeysToNames.put(89, "MEDIA_REWIND");
		mapKeysToNames.put(86, "MEDIA_STOP");
		mapKeysToNames.put(82, "MENU");
		mapKeysToNames.put(69, "MINUS");
		mapKeysToNames.put(91, "MUTE");
		mapKeysToNames.put(42, "N");
		mapKeysToNames.put(83, "NOTIFICATION");
		mapKeysToNames.put(78, "NUM");
		mapKeysToNames.put(43, "O");
		mapKeysToNames.put(44, "P");
		mapKeysToNames.put(56, "PERIOD");
		mapKeysToNames.put(81, "PLUS");
		mapKeysToNames.put(18, "POUND");
		mapKeysToNames.put(26, "POWER");
		mapKeysToNames.put(45, "Q");
		mapKeysToNames.put(46, "R");
		mapKeysToNames.put(72, "RIGHT_BRACKET");
		mapKeysToNames.put(47, "S");
		mapKeysToNames.put(84, "SEARCH");
		mapKeysToNames.put(74, "SEMICOLON");
		mapKeysToNames.put(59, "SHIFT_LEFT");
		mapKeysToNames.put(60, "SHIFT_RIGHT");
		mapKeysToNames.put(76, "SLASH");
		mapKeysToNames.put(1, "SOFT_LEFT");
		mapKeysToNames.put(2, "SOFT_RIGHT");
		mapKeysToNames.put(62, "SPACE");
		mapKeysToNames.put(17, "STAR");
		mapKeysToNames.put(63, "SYM");
		mapKeysToNames.put(48, "T");
		mapKeysToNames.put(61, "TAB");
		mapKeysToNames.put(49, "U");
		mapKeysToNames.put(0, "UNKNOWN");
		mapKeysToNames.put(50, "V");
		mapKeysToNames.put(25, "VOLUME_DOWN");
		mapKeysToNames.put(24, "VOLUME_UP");
		mapKeysToNames.put(51, "W");
		mapKeysToNames.put(52, "X");
		mapKeysToNames.put(53, "Y");
		mapKeysToNames.put(54, "Z");
		mapKeysToNames.put(16, "META_ALT_LEFT_ON");
		mapKeysToNames.put(2, "META_ALT_ON");
		mapKeysToNames.put(32, "META_ALT_RIGHT_ON");
		mapKeysToNames.put(64, "META_SHIFT_LEFT_ON");
		mapKeysToNames.put(1, "META_SHIFT_ON");
		mapKeysToNames.put(128, "META_SHIFT_RIGHT_ON");
		mapKeysToNames.put(4, "META_SYM_ON");
		mapKeysToNames.put(129, "CONTROL_LEFT");
		mapKeysToNames.put(130, "CONTROL_RIGHT");
		mapKeysToNames.put(131, "ESCAPE");
		mapKeysToNames.put(132, "END");
		mapKeysToNames.put(133, "INSERT");
		mapKeysToNames.put(92, "PAGE_UP");
		mapKeysToNames.put(93, "PAGE_DOWN");
		mapKeysToNames.put(94, "PICTSYMBOLS");
		mapKeysToNames.put(95, "SWITCH_CHARSET");
		mapKeysToNames.put(255, "BUTTON_CIRCLE");
		mapKeysToNames.put(96, "BUTTON_A");
		mapKeysToNames.put(97, "BUTTON_B");
		mapKeysToNames.put(98, "BUTTON_C");
		mapKeysToNames.put(99, "BUTTON_X");
		mapKeysToNames.put(100, "BUTTON_Y");
		mapKeysToNames.put(101, "BUTTON_Z");
		mapKeysToNames.put(102, "BUTTON_L1");
		mapKeysToNames.put(103, "BUTTON_R1");
		mapKeysToNames.put(104, "BUTTON_L2");
		mapKeysToNames.put(105, "BUTTON_R2");
		mapKeysToNames.put(106, "BUTTON_THUMBL");
		mapKeysToNames.put(107, "BUTTON_THUMBR");
		mapKeysToNames.put(108, "BUTTON_START");
		mapKeysToNames.put(109, "BUTTON_SELECT");
		mapKeysToNames.put(110, "BUTTON_MODE");
		mapKeysToNames.put(243, "COLON");
		mapKeysToNames.put(244, "F1");
		mapKeysToNames.put(245, "F2");
		mapKeysToNames.put(246, "F3");
		mapKeysToNames.put(247, "F4");
		mapKeysToNames.put(248, "F5");
		mapKeysToNames.put(249, "F6");
		mapKeysToNames.put(250, "F7");
		mapKeysToNames.put(251, "F8");
		mapKeysToNames.put(252, "F9");
		mapKeysToNames.put(253, "F10");
		mapKeysToNames.put(254, "F11");
		mapKeysToNames.put(255, "F12");
	}

	@Override
	public String getButtonToIndex(int index) {

		button = "";
		button = mapKeysToNames.get(index);
		return button;
	}
}
