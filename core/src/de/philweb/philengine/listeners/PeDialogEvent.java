package de.philweb.philengine.listeners;

public class PeDialogEvent extends PhilEngineEvent {

	private Type type;
	private IeventCallback cb;
	
	public void reset () {
		super.reset();
	}
	
	/** The type of input event. */
	public Type getType () {
		return type;
	}

	public void setType (Type type) {
		this.type = type;
	}
	
	public void setCallback(IeventCallback cb) {
		this.cb = cb;
	}
	public IeventCallback getCallback() {
		return cb;
	}
	
	public String toString () {
		return type.toString();
	}
	
	/** Types of low-level input events supported by stage2d. */
	static public enum Type {
		/** dialog play demo */
		playDemo,
		/** dialog play coop demo */
		purchasedDualPlayer,
		unassignedMenuController,
		unassignedControllers,
		googleAnalyticsOPTIN,
		googleAnalyticsOPTOUT
	}
}
