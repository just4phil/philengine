package de.philweb.philengine.listeners;

public interface PeEventListener {
	
	/** Try to handle the given event, if it is applicable.
	 * @return true if the event should be considered {@link Event#handle() handled} by scene2d. */
	public boolean handle (PhilEngineEvent event);
}
