package de.philweb.philengine.listeners;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

/** The base class for all events.
 * <p>
 * By default an event will "bubble" up through an actor's parent's handlers (see {@link #setBubbles(boolean)}).
 * <p>
 * An actor's capture listeners can {@link #stop()} an event to prevent child actors from seeing it.
 * <p>
 * An Event may be marked as "handled" which will end its propagation outside of the Stage (see {@link #handle()}). The default
 * {@link Actor#fire(Event)} will mark events handled if an {@link EventListener} returns true.
 * <p>
 * A cancelled event will be stopped and handled. Additionally, many actors will undo the side-effects of a canceled event. (See
 * {@link #cancel()}.) */
public class PhilEngineEvent {

//	public static final int 	
	
	private boolean handled; // true means the event was handled (the stage will eat the input)
	private boolean stopped; // true means event propagation was stopped
	private boolean cancelled; // true means propagation was stopped and any action that this event would cause should not happen
	
	
	/** Marks this event as handled. This does not affect event propagation inside scene2d, but causes the {@link Stage} event
	 * methods to return false, which will eat the event so it is not passed on to the application under the stage. */
	public void handle () {
		handled = true;
	}

	/** Marks this event cancelled. This {@link #handle() handles} the event and {@link #stop() stops} the event propagation. It
	 * also cancels any default action that would have been taken by the code that fired the event. Eg, if the event is for a
	 * checkbox being checked, cancelling the event could uncheck the checkbox. */
	public void cancel () {
		cancelled = true;
		stopped = true;
		handled = true;
	}

	/** Marks this event has being stopped. This halts event propagation. Any other listeners on the {@link #getListenerActor()
	 * listener actor} are notified, but after that no other listeners are notified. */
	public void stop () {
		stopped = true;
	}

	public void reset () {
		handled = false;
		stopped = false;
		cancelled = false;
	}
	
	/** {@link #handle()} */
	public boolean isHandled () {
		return handled;
	}

	/** @see #stop() */
	public boolean isStopped () {
		return stopped;
	}

	/** @see #cancel() */
	public boolean isCancelled () {
		return cancelled;
	}
}
