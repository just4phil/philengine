package de.philweb.philengine.listeners;

public class PeAssignMenuControllerEvent extends PeDialogEvent {


	/** Types of low-level input events supported by stage2d. */
	static public enum Type {
		/** dialog play demo */
		playDemo,
		/** dialog play coop demo */
		purchasedDualPlayer,
		unassignedMenuController,
		unassignedControllers,
		googleAnalyticsOPTIN,
		googleAnalyticsOPTOUT
	}
}
