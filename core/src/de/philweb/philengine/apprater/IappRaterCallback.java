package de.philweb.philengine.apprater;

 /** Callback class for the appRater.
 * defines the hook method to open the app rater dialog
 * 
 * @author just4phil - heavily.loaded.games@googlemail.com */

public interface IappRaterCallback {

	public void openAppRaterDialog ();
	public void callBackButtonYES ();
	public void callBackButtonNO ();
}
