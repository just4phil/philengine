package de.philweb.philengine.apprater;

public class AppRaterCallbackAdapter implements IappRaterCallback {

	@Override
	public void openAppRaterDialog() {}

	@Override
	public void callBackButtonYES() {}

	@Override
	public void callBackButtonNO() {}
}
