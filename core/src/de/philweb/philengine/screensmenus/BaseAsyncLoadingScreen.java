package de.philweb.philengine.screensmenus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.philweb.philengine.common.Game;
import de.philweb.philengine.common.ResolutionFileResolverChooser;
import de.philweb.philengine.common.ResolutionFileResolverChooser.ChooserStrategy;
import de.philweb.philengine.common.ResolutionFileResolverChooser.Resolution;


public abstract class BaseAsyncLoadingScreen extends AbstractScreen {

	String last_version;
	public static String resolvedPath;
	Texture background;
	TextureRegion backgroundRegion;
	TextureRegion whitePixel;
	Color color;
	float oldRed;
	float oldGreen;
	float oldBlue;
	float oldAlpha;
	float progress = 0f;

	// progressBar
	float pbX;
	float pbY;
	float pbWidthFactor;
	float pbHeight;
	
	ChooserStrategy chooserStrategy;
	Resolution[] resolutions;
	String path;
	
	public BaseAsyncLoadingScreen (Game game) {
		super(game);
				
		configureProgressBar(319.5f, 92.2f, 215f, 20.5f); // just set defaults... user should customize this!
		
		//--- configuer asset loading
		configureSplashLoader();
				
		//--------------------------------------------------		
//		game.start = System.currentTimeMillis(); 	 // just 4 debugging time metrics
				
		//----- load resolution dependend splash -------------------------------
		ResolutionFileResolverChooser resolver = new ResolutionFileResolverChooser(new InternalFileHandleResolver(), chooserStrategy, resolutions);
		
		resolvedPath = resolver.resolve(path).path();
		if (game.getDebugMode()) Gdx.app.log("resolvedPath", "" + resolvedPath);
				
		background = new Texture(Gdx.files.internal(resolvedPath), true);
		background.setFilter(TextureFilter.Linear, TextureFilter.Linear);
      		
		if (resolvedPath.equals("imageatlas/480/HLGsplash.png")) {				// FIXME // TODO .... hard coded!!
			backgroundRegion = new TextureRegion(background, 0, 0, 854, 480);
			whitePixel = new TextureRegion(background, 318, 366, 1, 1);	
		}
		else if (resolvedPath.equals("imageatlas/720/HLGsplash.png")) {
			backgroundRegion = new TextureRegion(background, 0, 0, 1280, 720);	
			whitePixel = new TextureRegion(background, 476, 549, 1, 1);	
		}
		else if (resolvedPath.equals("imageatlas/1080/HLGsplash.png")) {
			backgroundRegion = new TextureRegion(background, 0, 0, 1920, 1080);	
			whitePixel = new TextureRegion(background, 714, 823, 1, 1);	
		}
		
//		versionFont = new BitmapFont(Gdx.files.internal("fonts/ComicRelief-32.fnt"), Gdx.files.internal("fonts/comicrelief32.png"), false);
//		versionFont.setUseIntegerPositions(false);		
//		versionFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
						
		//--- do stuff before loading assets ---------------
		doBeforeAssetLoading();
		
		//--- start asset loading for game ---------------
		loadAssets();
	}
	//===================================================================================
	
	protected void setChooserStrategy(ChooserStrategy chooserStrategy) {
		this.chooserStrategy = chooserStrategy;
	}
	protected void setResolutions(Resolution[] resolutions) {
		this.resolutions = resolutions;
	}
	protected void setSplashPath(String path) {
		this.path = path;
	}
	protected void configureProgressBar(float pbX, float pbY, float pbWidthFactor, float pbHeight) {
		this.pbX = pbX;
		this.pbY = pbY;
		this.pbWidthFactor = pbWidthFactor;
		this.pbHeight = pbHeight;
	}
	
	public abstract void configureSplashLoader();
	public abstract void doBeforeAssetLoading();
	public abstract void loadAssets();
	    
	@Override
	public void update (float deltaTime) {			
		
		if (Gdx.input.isKeyPressed(Keys.BACK)){		// back-key: zur�ck
//			background.dispose();
			Gdx.app.exit();
			return;
		}
		progress = getAssetService().getProgress();	
	}
	
	@Override
	public void present () {
		game.getCameraManager().useHUDCam();
		batch.begin();
		batch.draw(backgroundRegion, 0, 0, game.getVIRTUAL_WIDTH_HUD(), game.getVIRTUAL_HEIGHT_HUD());
		
    	//======== progress bar zeichnen ===========================
		//--- zuerst originalfarben speichern
		color = batch.getColor();//get current Color, you can't modify directly
		oldAlpha = color.a;
		oldRed = color.r;
		oldGreen = color.g; //save its green
		oldBlue = color.b; //save its blue
		
    	//---- color auf gr�n
		color.r = 1.0f; 
		color.g = 0.8f; 
		color.b = 0.02f;
		color.a = 1.0f;
		batch.setColor(color); //set it
		
//		batch.draw(whitePixel, 319.5f, 92.2f, 215f * progress, 20.5f);	//---- gr�nen balken in abh�ngigkeit von progress stretchen
//		batch.draw(whitePixel, 299.5f, 92.2f, 201f * progress, 20.5f);	//---- gr�nen balken in abh�ngigkeit von progress stretchen
		batch.draw(whitePixel, pbX, pbY, pbWidthFactor * progress, pbHeight);	//---- gr�nen balken in abh�ngigkeit von progress stretchen
		
	  	//---- farbe wieder auf original (weiss)
		color.r = 1f;
		color.g = 1f;
		color.b = 1f;
		color.a = 1f;
		batch.setColor(color); //set it	
	    batch.end();
	    
	    
		//---- draw version info ----------------------
		
//		versionFontScaleX = versionFont.getScaleX();
//		versionFontScaleY = versionFont.getScaleY();
//		versionFont.setScale(versionFontScaleX * 0.50f, versionFontScaleY * 0.50f);
//			
//		if (bubblr.version_prefs.equals(Game2.VERSION)) {
//			float stringwidth = versionFont.getBounds("version: " + Bubblr.VERSION).width;
//			versionFont.draw(batcher, "version: " + Bubblr.VERSION, 400 - stringwidth * 0.5f, 40); // x: 550
//		}
//		else {
//			float stringwidth = versionFont.getBounds("new version: " + Bubblr.VERSION).width;
//			float stringwidth2 = versionFont.getBounds("last version: " + bubblr.version_prefs).width;
//			versionFont.draw(batcher, "new version: " + Bubblr.VERSION, 400 - stringwidth * 0.5f, 40);
//			versionFont.draw(batcher, "last version: " + bubblr.version_prefs, 400 - stringwidth2 * 0.5f, 20);
//		}
//		
//		versionFont.setScale(versionFontScaleX * 1f, versionFontScaleY * 1f);
//		
//	  	//---- farbe wieder auf original (weiss)
//		color.r = oldRed;
//		color.g = oldGreen;
//		color.b = oldBlue;
//		color.a = oldAlpha;
//			
//		batch.setColor(color); //set it
	}

	@Override
	public void pause () {
		super.pause();	//-------- important to pause music ---------------
	}

	@Override
	public void resume () {
	}

	@Override
	public void dispose () {
		super.dispose();
		background.dispose();
//		versionFont.dispose();
	}
}
