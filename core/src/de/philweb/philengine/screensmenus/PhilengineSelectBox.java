package de.philweb.philengine.screensmenus;
//package de.philweb.philengine.tools;
//
//import com.badlogic.gdx.graphics.Color;
//import com.badlogic.gdx.graphics.g2d.Batch;
//import com.badlogic.gdx.scenes.scene2d.InputEvent;
//import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
//import com.badlogic.gdx.scenes.scene2d.ui.List;
//import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
//import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
//import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
//
//import de.philweb.bubblr2HD.screens.AbstractScreen;
//
//	
//public class PhilengineSelectBox extends SelectBox implements IphilengineUImethods {
//
//	MenuNavigationManager menuNavigationManager;
//	public int navigationIndex;
//	int backupNavigationIndex = 0;
//	public boolean navigationIsActive;
//	public int listSize;
//	Color color;
//	float oldRed;
//	float oldGreen;
//	float oldBlue;
//	float oldAlpha;
//	float oldScaleX;
//	float oldScaleY;
//	public String[] items2;
//	AbstractScreen screen;
//	
//	List list;
//	ScrollPane scrollPane;
//	float itemHeight;
//	
//	
//	public PhilengineSelectBox(Object[] items, SelectBoxStyle style, MenuNavigationManager menuNavigationManager, AbstractScreen screen) {
//		super(items, style);
//
//		this.menuNavigationManager = menuNavigationManager;
//		this.screen = screen;
//		this.items2 = (String[]) items;
//		this.listSize = items.length;
//		navigationIndex = -1;
//		navigationIsActive = false;
//		
////		list = getList();
////		scrollPane = (ScrollPane)list.getParent();
//		
//		
//		//----- aktivieren der box, damit rote schrift nach klick/touch gezeigt wird ---
//		//--- funktioniert teilweise, aber macht probleme weil es beim umschalten auf die liste der selectbox zu einem konflikt kommt
////		addListener(new InputListener() {
////	        public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
////	        	
////	        	if (PhilengineSelectBox.this.menuNavigationManager.selectBoxIsOpened == false) {
////	        		PhilengineSelectBox.this.menuNavigationManager.activateNavigationEntry(PhilengineSelectBox.this.menuNavigationManager.getSelectBoxNavigationIndex(PhilengineSelectBox.this));
////	        	}
////				return true;
////	        }
////		});
//	}
//
//	
//	
//	public void open() {
//
//		InputEvent input = new InputEvent();
//		input.setType(Type.touchDown);
//		input.setButton(0);
//		input.setPointer(1);
//		fire(input);
//		
////		getList().getParent().show(getStage());
//		
//		backupNavigationIndex = navigationIndex;
//		navigationIndex = 0;
//	}
//	
//	
//	public void close() {
//			
//		hideList();
//		navigationIndex = backupNavigationIndex;
//	}
//	
//	
//	
//	public void select(int selectionIndex) {
//		
//		setSelection(selectionIndex);
//		ChangeEvent change = new ChangeEvent();
//		fire(change);
//	}
//
//
//
//	
//	
//	public void updateItems(Object[] items) {
//		
//		this.items2 = (String[]) items;
//		this.listSize = items.length;
//		setItems(items2);
//	}
//	
//	
//	
//
//	@Override
//	public void draw(Batch batch, float parentAlpha) {
//
//		// If text set and intented to be used
//		// ##################################################################
//		if (navigationIsActive) {
//			
//			super.draw(batch, parentAlpha);
//			
//			oldScaleX = getStyle().font.getScaleX();
//			oldScaleY = getStyle().font.getScaleY();
//			
//			
//			color = getStyle().font.getColor();//get current Color, you can't modify directly  
//			oldAlpha = color.a; //save its alpha
//			oldRed = color.r;
//			oldGreen = color.g; //save its green
//			oldBlue = color.b; //save its blue
//			
//			color.r = 1.0f;
//			color.g = 0f;
//			color.b = 0f;
//			color.a = 1.0f;
//			
//			getStyle().font.setColor(color.r, color.g, color.b, color.a);
//
//			
//			//---- draw font in red -------------------------
//			getStyle().font.draw(batch, getSelection(), 
//					getX() + 4, // + getWidth()/2 - getStyle().font.getBounds(getSelection()).width/2,
//					getY() + getHeight()/2 + getStyle().font.getBounds(getSelection()).height/2);
//			
//			
//			list = getList();
//			
//			if (list != null) {
//
//				if (menuNavigationManager.isNavigationItemChanged() == true) {
//					
//					//---- highlight the selected item in list ---------------
//					list.setSelectedIndex(menuNavigationManager.navigationIsActiveEntry);
//					
//					//---- TODO: scroll the highlighted item in the middle --------					
//					scrollPane = (ScrollPane)list.getParent();
//					itemHeight = list.getItemHeight();
//					scrollPane.scrollToCenter(0,  itemHeight * (menuNavigationManager.navigationEntryCounter - menuNavigationManager.navigationIsActiveEntry - 1), 0, itemHeight);
//				}
//			}
//			
//
//			color.a = oldAlpha;
//			color.r = oldRed;
//			color.g = oldGreen;
//			color.b = oldBlue;
//			getStyle().font.setColor(color.r, color.g, color.b, color.a);
//				
//			getStyle().font.setScale(oldScaleX, oldScaleY);	
//			
//		}
//
//		// Draw default
//		// ##################################################################
//		else {
//			super.draw(batch, parentAlpha);
//		}
//	}
//	
//	
//	public boolean isOpen() {
//
//		if (getList() != null) return true;
//		return false;
//	}
//
//
//@Override
//public void setNavigationIsActive (boolean isActive) {
//	navigationIsActive = isActive;
//}
//
//@Override
//public boolean getNavigationIsActive () {
//	return navigationIsActive;
//}
//
//@Override
//public int getNavigationIndex() {
//	return navigationIndex;
//}

//}

