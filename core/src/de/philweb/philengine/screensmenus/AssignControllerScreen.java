package de.philweb.philengine.screensmenus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Texture;

import de.philweb.philengine.common.Game;
import de.philweb.philengine.controllers.AbstractController;
import de.philweb.philengine.controllers.MgrControllerConfigManager;
import de.philweb.philengine.listeners.IeventCallback;

public class AssignControllerScreen extends AbstractScreen {

	public final static int TYPENONE 	= 0;
	public final static int TYPEAXIS 	= 1;
	public final static int TYPEBUTTON 	= 2;
	public final static int TYPEKEY 	= 3;
	public final static int TYPETOUCH 	= 4;
	
	private final static int INITIALBUTTONINDEX = -999;
	public static final float MINCONTROLLERAXISVALUE = 0.1f;
	
	Texture controllerAssignScreenTexture;
	
	int counter;
	boolean isProcessStarted = false;
	AbstractController gameController = null;
	Controller controller;
	boolean readyToAssign = false; // if button is axis then wait until back to 0 !
	long lastAxisValue;

	int type;
	int index;
	float value;
	
	InputMultiplexer inputMultiplexer;
	ControllerAdapter controllerAdapter;
	IeventCallback eventCallback;
	
	public AssignControllerScreen(Game game, IeventCallback cb) {
		super(game);
		this.eventCallback = cb;
		
		if (game.getDebugMode()) Gdx.app.log("AssignControllerScreen", "constructed");
		controllerAssignScreenTexture = game.getControllerConfigManager().controllerAssignScreenTexture;
		
		initializeControllerValues();// ---- reset controllerListener Values ----
		//--------------------
		
		controllerAdapter = new ControllerAdapter() {

			@Override
			public void connected (Controller controller) {
//				AssignControllerScreen.this.game.getPlatformResolver().sendTrackerEvent("Controllers", "new Controller connected", "", (long)1);
				AssignControllerScreen.this.game.getControllerConfigManager().updateGamePadControllers();
			}
			@Override
			public boolean axisMoved (Controller controller, int axisIndex, float value) {
//				// for testing
//				if (value > 0.75f || value < -0.75f) {
//					Gdx.app.log("", "controller: " + controller.getName() + " // axisIndex: " + axisIndex + " // value: " + value);
//				}
				if (value > MINCONTROLLERAXISVALUE || value < -MINCONTROLLERAXISVALUE) {
					// AssignControllerScreen.this.game.getPlatformResolver().showToast("controller: " + controller.getName() +
					// " // axisIndex: " + axisIndex + " // value: " + value);
					return buttonAndAxis(controller, TYPEAXIS, axisIndex, value);
				}
				return false;
			}
			@Override
			public boolean buttonUp (Controller controller, int buttonIndex) {
				// ---- if controllers should be assigned .... forward the input ......
				return buttonAndAxis(controller, TYPEBUTTON, buttonIndex, 0f);
			}
		};
		
		Controllers.addListener(controllerAdapter);	
		
		// ==== used to detect fire tv remote as a controller on amazon fire tv ===============
		InputAdapter inputProcessorAssignController = new InputAdapter() {
			@Override
			public boolean touchUp (int screenX, int screenY, int pointer, int button) {
				return buttonAndAxis(null, TYPETOUCH, 0, 0f);
			}
			@Override
			public boolean keyUp (int keycode) {
				Gdx.app.log("AssignControllerScreen", "keyUp: " + keycode);
				return buttonAndAxis(null, TYPEKEY, keycode, 0f);
			}
		};
		
		inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(inputProcessorAssignController);
		inputMultiplexer.addProcessor(stage);
		Gdx.input.setInputProcessor(inputMultiplexer);
	}
		
	void initializeControllerValues () {
//		if (game.getDebugMode()) Gdx.app.log("AssignControllerScreen", "initializeControllerValues()");
		controller = null;
		index = INITIALBUTTONINDEX;
		value = 0f;
		type = TYPENONE;
		gameController = null;
		readyToAssign = false;
	}
		
	public boolean buttonAndAxis (Controller controller, int type, int index, float value) {
		this.controller = controller;
		this.index = index;
		this.value = value;
		this.type = type;

		if (type == TYPETOUCH) { // ---- for real game controllers ---------
			this.gameController = game.getControllerConfigManager().getController(MgrControllerConfigManager.ControllerAndroid, MgrControllerConfigManager.ControllerAndroid, 1);
			if (this.gameController != null) readyToAssign = true;
		}
		else if (type == TYPEBUTTON) { // ---- for real game controllers ---------
			this.gameController = game.getControllerConfigManager().getInputController(controller);
			readyToAssign = isConfigOK(gameController);
		}
		else if (type == TYPEKEY) { // ---- for key controllers like amazon fire tv REMOTE (controller == null) ---------
			if (game.getAppStore() == Game.APPSTORE_AMAZON) {
				this.gameController = game.getControllerConfigManager().getController(MgrControllerConfigManager.ControllerFireTVRemote, MgrControllerConfigManager.ControllerFireTVRemote, 1);
				if (this.gameController != null) readyToAssign = true;
			}
			else if (game.getAppStore() == Game.APPSTORE_DESKTOP) {
				this.gameController = game.getControllerConfigManager().getController(MgrControllerConfigManager.ControllerDesktopPL1, MgrControllerConfigManager.ControllerDesktopPL1, 1);
				if (this.gameController != null) readyToAssign = true;
			}
		}
		return readyToAssign;
	}

	boolean isConfigOK (AbstractController gameController) {
		if (gameController.controllerConfig == null) {
			game.getPlatformResolver().showToast("your controller is not supported at the moment!", false);
			if (game.getDebugMode()) Gdx.app.log("AssignControllerScreen", "your controller is not supported at the moment!");
			initializeControllerValues();
			return false;
		}
		return true;
	}

	@Override
	public void update (float delta) {

		if (inputSource == null) {
			if (isProcessStarted == false) {
				game.getPlatformResolver().showToast("press a key or button", false);
				isProcessStarted = true;
			} 
			else {
				if (readyToAssign) {
					if (game.getDebugMode()) Gdx.app.log("AssignControllerScreen", "readyToAssign");
					game.getControllerConfigManager().setMenuController(gameController);
					inputSource = game.getControllerConfigManager().getMenuController();
					if (inputSource != null) {
						game.getPlatformResolver().showToast("ok, got it!", false);
						if (game.getDebugMode()) Gdx.app.log("AssignControllerScreen", "ok, got it!");
					}
					else if (game.getDebugMode()) Gdx.app.log("AssignControllerScreen", "still no controller assigned :(");
				}
				isProcessStarted = false;				
				initializeControllerValues();// ---- reset controllerListener Values ----
			}
		}
		else {
			if (game.getDebugMode()) Gdx.app.log("AssignControllerScreen", "menucontroller found -> set last screen again");
			if (eventCallback != null) eventCallback.doAfterEventProcessing();
			game.setScreen(game.temporarilySavedScreenWhileControllerAssignScreenIsOn);
		}
	}
	
	@Override
	public void present() {
		if (controllerAssignScreenTexture != null) {
			game.getCameraManager().useHUDCam();
			batch.begin();
			batch.draw(controllerAssignScreenTexture, 
					(game.getVIRTUAL_WIDTH_HUD() * 0.5f) - 100f, 
					(game.getVIRTUAL_HEIGHT_HUD() * 0.5f) - 70f,
					200f, 140f);
			batch.end();
		}
	}

	@Override
	public void doAfterSetScreen() {
		initializeControllerValues();// ---- reset controllerListener Values ----
	}
	
	@Override
	public void dispose() {
		Gdx.input.setInputProcessor(stage);
		inputMultiplexer = null;
		Controllers.removeListener(controllerAdapter);	
		if (game.getDebugMode()) Gdx.app.log("AssignControllerScreen", "disposed listeners etc");
	}
}
