package de.philweb.philengine.screensmenus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.philweb.philengine.common.Game;
import de.philweb.philengine.common.ResolutionFileResolverChooser;
import de.philweb.philengine.common.ResolutionFileResolverChooser.ChooserStrategy;
import de.philweb.philengine.common.ResolutionFileResolverChooser.Resolution;


public abstract class BaseLoadingScreen extends AbstractScreen {

	String last_version;
	public static String resolvedPath;
	Texture background;
	TextureRegion backgroundRegion;
		 
	ChooserStrategy chooserStrategy;
	Resolution[] resolutions;
	String path;
	boolean renderedOnce = false;
	boolean loadedOnce = false;
	
	public BaseLoadingScreen (Game game) {
		super(game);
				
		//--- configuer asset loading
		configureSplashLoader();
				
		//--------------------------------------------------		
//		game.start = System.currentTimeMillis(); 	 // just 4 debugging time metrics
				
		//----- load resolution dependend splash -------------------------------
		ResolutionFileResolverChooser resolver = new ResolutionFileResolverChooser(new InternalFileHandleResolver(), chooserStrategy, resolutions);
		
		resolvedPath = resolver.resolve(path).path();
		if (game.getDebugMode()) Gdx.app.log("resolvedPath", "" + resolvedPath);
				
		background = new Texture(Gdx.files.internal(resolvedPath), true);
		background.setFilter(TextureFilter.Linear, TextureFilter.Linear);
      		
		if (resolvedPath.equals("imageatlas/480/HLGsplash.png")) {					// FIXME // TODO .... hard coded!!
			backgroundRegion = new TextureRegion(background, 0, 0, 854, 480);
		}
		else if (resolvedPath.equals("imageatlas/720/HLGsplash.png")) {
			backgroundRegion = new TextureRegion(background, 0, 0, 1280, 720);	
		}
		else if (resolvedPath.equals("imageatlas/1080/HLGsplash.png")) {
			backgroundRegion = new TextureRegion(background, 0, 0, 1920, 1080);	
		}
		
//		versionFont = new BitmapFont(Gdx.files.internal("fonts/ComicRelief-32.fnt"), Gdx.files.internal("fonts/comicrelief32.png"), false);
//		versionFont.setUseIntegerPositions(false);		
//		versionFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
						
		//--- do stuff before loading assets ---------------
		doBeforeAssetLoading();
	}
	//===================================================================================
	
	protected void setChooserStrategy(ChooserStrategy chooserStrategy) {
		this.chooserStrategy = chooserStrategy;
	}
	protected void setResolutions(Resolution[] resolutions) {
		this.resolutions = resolutions;
	}
	protected void setSplashPath(String path) {
		this.path = path;
	}
	
	public abstract void configureSplashLoader();
	public abstract void doBeforeAssetLoading();
	public abstract void loadAssets();
	    	
	@Override
	public void update (float delta) {
		//--- start asset loading for game ---------------
		if (renderedOnce && loadedOnce == false) { 
			loadAssets();
			loadedOnce = true;
		}
	}
	
	@Override
	public void present () {
		game.getCameraManager().useHUDCam();
		batch.begin();
		batch.draw(backgroundRegion, 0, 0, game.getVIRTUAL_WIDTH_HUD(), game.getVIRTUAL_HEIGHT_HUD());
	    batch.end();
	    
	    
		//---- draw version info ----------------------
		
//		versionFontScaleX = versionFont.getScaleX();
//		versionFontScaleY = versionFont.getScaleY();
//		versionFont.setScale(versionFontScaleX * 0.50f, versionFontScaleY * 0.50f);
//			
//		if (bubblr.version_prefs.equals(Game2.VERSION)) {
//			float stringwidth = versionFont.getBounds("version: " + Bubblr.VERSION).width;
//			versionFont.draw(batcher, "version: " + Bubblr.VERSION, 400 - stringwidth * 0.5f, 40); // x: 550
//		}
//		else {
//			float stringwidth = versionFont.getBounds("new version: " + Bubblr.VERSION).width;
//			float stringwidth2 = versionFont.getBounds("last version: " + bubblr.version_prefs).width;
//			versionFont.draw(batcher, "new version: " + Bubblr.VERSION, 400 - stringwidth * 0.5f, 40);
//			versionFont.draw(batcher, "last version: " + bubblr.version_prefs, 400 - stringwidth2 * 0.5f, 20);
//		}
//		
//		versionFont.setScale(versionFontScaleX * 1f, versionFontScaleY * 1f);
//		
//	  	//---- farbe wieder auf original (weiss)
//		color.r = oldRed;
//		color.g = oldGreen;
//		color.b = oldBlue;
//		color.a = oldAlpha;
//			
//		batch.setColor(color); //set it
	    
	    
	    renderedOnce = true;
	}

	@Override
	public void pause () {
		super.pause();	//-------- important to pause music ---------------
	}

	@Override
	public void resume () {
	}

	@Override
	public void dispose () {
		super.dispose();
		background.dispose();
//		versionFont.dispose();
	}
}
