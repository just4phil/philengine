package de.philweb.philengine.screensmenus;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;

import de.philweb.philengine.drawables.FixedTextureRegionDrawable;

public class PhilengineSlider extends Slider implements IphilengineUIActors {

	public int navigationIndex;
	public boolean navigationIsActive;
	float stepSize;
	AbstractSubMenu parentMenu = null;
	FixedTextureRegionDrawable knob;
	FixedTextureRegionDrawable knobActivated;
	FixedTextureRegionDrawable backgroundGrau;
	FixedTextureRegionDrawable backgroundRot;
	boolean isActivated = false; // if slider is selected and enter is pressed then it isActivated and ready to be changed
	private int enterMenuEntry;		// used to store the entry for enterMenu
	private Object enterMenuObject;	// used to store the entry for enterMenu
	
	public PhilengineSlider(float min, float max, float stepSize, boolean vertical, Skin skin) {
		super(min, max, stepSize, vertical, skin);

		this.stepSize = stepSize; 
		navigationIndex = -1;
		navigationIsActive = false;
	}
		
	public void setupGraphics(FixedTextureRegionDrawable knob, FixedTextureRegionDrawable knobActivated, 
		FixedTextureRegionDrawable backgroundGrau, FixedTextureRegionDrawable backgroundRot) {
		
		this.knob = knob;
		getStyle().knob = knob;
		this.knobActivated = knobActivated;
		this.backgroundGrau = backgroundGrau;
		getStyle().background = backgroundGrau;
		this.backgroundRot = backgroundRot;
	}
	
	public void incrementValue() {
		
		float value = getValue();
		value = value + stepSize;
		if (value > getMaxValue()) value = getMaxValue();
		setValue(value);
	}
	
	public void decrementValue() {
		
		float value = getValue();
		value = value - stepSize;
		if (value < getMinValue()) value = getMinValue();
		setValue(value);
	}
	
	
    @Override
    public void draw (Batch batch, float parentAlpha) {

    	if (navigationIsActive) getStyle().background = backgroundRot;
    	if (isActivated) getStyle().knob = knobActivated;
    	
    	super.draw(batch, parentAlpha);
    	
    	if (navigationIsActive) getStyle().background = backgroundGrau;
    	if (isActivated) getStyle().knob = knob;
    }
    
 	@Override
 	public void setNavigationIsActive (boolean isActive) {
 		navigationIsActive = isActive;
 	}

 	@Override
 	public boolean getNavigationIsActive () {
 		return navigationIsActive;
 	}
 	
 	@Override
 	public int getNavigationIndex() {
 		return navigationIndex;
 	}

	@Override
	public void addEnterMenuListener(int enterMenuEntry) {
		addEnterMenuListener(enterMenuEntry, null);
	}
	
	@Override
	public void addEnterMenuListener(int enterMenuEntry, Object enterMenuObject) {
		this.enterMenuEntry = enterMenuEntry;
		this.enterMenuObject = enterMenuObject;
		addEnterMenuListener(
				enterMenuEntry, 
				null, 
				new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						parentMenu.enterMenu(getEnterMenuEntry(), getEnterMenuObject());
					}
				});
	}
	
	@Override
	public void addEnterMenuListener(int enterMenuEntry, Object enterMenuObject, ChangeListener listener) {
		this.enterMenuEntry = enterMenuEntry;
		this.enterMenuObject = enterMenuObject;
		addListener(listener);
	}
	
	@Override
	public int getEnterMenuEntry() {
		return enterMenuEntry;
	}
//	@Override
//	public void setEnterMenuObject(Object enterMenuObject) {
//		this.enterMenuObject = enterMenuObject;
//	}
	@Override
	public Object getEnterMenuObject() {
		return enterMenuObject;
	}
}
