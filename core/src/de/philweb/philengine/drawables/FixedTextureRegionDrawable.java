package de.philweb.philengine.drawables;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;

	/** Drawable for a {@link TextureRegion} with fixed width/height. 
	 * This can be useful if assets should be rendered on a virtual resolution and must be rendered 
	 * in the size of the original asset (regardless how the real size is) relatively to the virt. res.
	 * if w / h = 0 then getRegionWidth() / getRegionHeight() will be used
	 * @author Andre Schmode */
	public class FixedTextureRegionDrawable extends BaseDrawable {
		
		private TextureRegion region;
		private float w;
		private float h;
		private float draw_w;
		private float draw_h;
		
		public FixedTextureRegionDrawable (TextureRegion region, float w, float h) {
			this.w = w;
			this.h = h;
			setRegion(region);
		}
		
		@Override
		public void draw (Batch batch, float x, float y, float width, float height) {
			if (w > 0) draw_w = w;
			else draw_w = width;
			
			if (h > 0) draw_h = h;
			else draw_h = height;
			
			if (region != null) batch.draw(region, x, y, draw_w, draw_h);
		}
		
		public void draw (Batch batch, float x, float y) {
			if (region != null) batch.draw(region, x, y, w, h);
		}
		
		//---- hiermit kann "normal" gerendert werden .... width/height wird dann beruecksichtigt
		//--- wird benoetigt damit im menu der rote backbutton kleiner umrandet werden kann (wenn aktiv)
		public void draw (Batch batch, float x, float y, float width, float height, boolean drawScaled) {
			if (region != null) batch.draw(region, x, y, width, height);
		}
		
		public void setRegion (TextureRegion region) {
			this.region = region;
			
			if (w > 0) setMinWidth(w);
			else setMinWidth(region.getRegionWidth());
			
			if (h > 0) setMinHeight(h);
			else setMinHeight(region.getRegionHeight());
		}
		
		public TextureRegion getRegion () {
			return region;
		}
		
		/** returns the fixed width */
		public float getW() {
			return w;
		}
		/** returns the fixed height */
		public float getH() {
			return h;
		}
	}

